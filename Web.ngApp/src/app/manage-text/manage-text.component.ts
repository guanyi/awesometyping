import { Component, OnInit, Injectable } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { AuthorizeService } from '../../api-authorization/authorize.service';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { Globals } from '../globals';
import { PracticeText } from '../shared/models/index';
import { PracticeTextService, DataExchangeService } from '../shared/services/index';
import { ToastrService } from 'ngx-toastr';
import { Column, GridOption, Formatters, OnEventArgs } from 'angular-slickgrid';

@Component({
  selector: 'app-manage-text',
  templateUrl: './manage-text.component.html',
  styleUrls: ['./manage-text.component.css']
})

@Injectable()
export class ManageTextComponent implements OnInit {
  private regex = /\s+/gi;
  private autoReformat = true;
  public isAuthenticated: Observable<boolean>;
  currentUserName: string;
  practiceText: PracticeText;
  selectedPracticeText: PracticeText;
  practiceTextList: PracticeText[];
  practiceTextForm: FormGroup;
  columnDefinitions: Column[] = [];
  gridOptions: GridOption = {};
  onlyShowBestPractice = false;

  constructor(
    private authorizeService: AuthorizeService,
    private fb: FormBuilder,
    private practiceTextService: PracticeTextService,
    private dataExchangeService: DataExchangeService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.isAuthenticated = this.authorizeService.isAuthenticated();
    this.authorizeService.getUser()
      .pipe(map(u => u && u.name))
      .subscribe(user => this.currentUserName = user);
    this.practiceText = this.createEmptyPracticeText();
    this.buildForm(this.practiceText);
    this.prepareGridColumns();
    // fill the dataset with data
    this.getAllPracticeText();
  }

  buildForm(practiceText: PracticeText) {
    this.practiceTextForm = this.fb.group({
      id: [practiceText.id],
      title: [practiceText.title],
      text: [practiceText.text],
      userName: this.currentUserName,
      isPublic: false
    });
  }

  updateTitle(event: any) {
    this.practiceText.title = event.target.value.trim();
  }

  updateText(event: any) {
    this.practiceText.text = event.target.value;
    if (this.autoReformat) {
      this.autoReformatPracticeText();
    }
  }

  save() {
    if (this.isRequiredFieldsEmpty()) {
      this.toastr.error('Both title and text fields are required');
      return;
    }
    this.practiceText.userName = this.currentUserName;
    this.practiceText.totalWords = this.practiceText.text.split(' ').length;
    this.practiceText.totalCharacters = this.practiceText.text.length;
    if (this.practiceText.id === Globals.emptyGuid) {
      this.practiceTextService.createPracticeText(this.practiceText)
        .subscribe(practiceText => {
          this.practiceText = practiceText;
          this.toastr.success('Text added successfully');
          this.clearAddTextForm();
          this.practiceTextList.push(this.practiceText);
        }, error => this.toastr.error('Add text failed, try again'));
    } else {
      this.practiceTextService.updatePracticeText(this.practiceText)
        .subscribe(practiceText => {
          this.practiceText = practiceText;
          this.toastr.success('Text updated successfully');
        }, error => this.toastr.error('Update text failed, try again'));
    }
  }

  createEmptyPracticeText(): PracticeText {
    const emptyPracticeText = {
      id:  Globals.emptyGuid,
      title: '',
      text: '',
      totalWords: 0,
      totalCharacters: 0,
      isPublic: false,
      userName: '',
      modifiedDate: new Date()
    };
    return emptyPracticeText;
  }

  clearAddTextForm() {
    this.practiceTextForm.get('title').setValue('');
    this.practiceTextForm.get('text').setValue('');
  }

  autoReformatPracticeText() {
    this.practiceText.text = this.practiceText.text.trim().replace(this.regex, ' ');
  }

  onSelectedRowsChanged(e, args) {
    if (Array.isArray(args.rows)) {
      args.rows.map( idx => {
        this.selectedPracticeText = args.grid.getDataItem(idx);
        this.dataExchangeService.loadPracticeTextToType(this.selectedPracticeText);
      });
    }
  }

  loadEmptyPracticeText() {
    this.clearAddTextForm();
    this.practiceText.id = Globals.emptyGuid;
  }

  loadSelectedPracticeText() {
    this.practiceTextForm.get('title').setValue(this.selectedPracticeText.title);
    this.practiceTextForm.get('text').setValue(this.selectedPracticeText.text);
    this.practiceText = this.selectedPracticeText;
  }

  isRequiredFieldsEmpty() {
    const title = this.practiceTextForm.get('title').value;
    const text = this.practiceTextForm.get('text').value;
    return(title.length === 0 || !title.trim() || text.length === 0 || !text.trim());
  }

  getAllPracticeText() {
    this.practiceTextService.getAllPracticeTextForAUser(this.currentUserName)
    .subscribe(response => {
      this.practiceTextList = response['result'];
    }, error => this.toastr.error('Get text failed, try again'));
  }

  getBestPracticeText() {
    this.practiceTextService.getBestPracticeTextForAUser(this.currentUserName)
    .subscribe(response => {
      this.practiceTextList = response['result'];
    }, error => this.toastr.error('Get text failed, try again'));
  }

  toogleBestPracticeText() {
    if (this.onlyShowBestPractice === false) {
      this.getBestPracticeText();
      this.onlyShowBestPractice = true;
    } else {
      this.getAllPracticeText();
      this.onlyShowBestPractice = false;
    }
  }

  prepareGridColumns() {
    this.columnDefinitions = [
      { id: 'delete', field: 'id', excludeFromHeaderMenu: true, formatter: Formatters.deleteIcon, minWidth: 30, maxWidth: 30,
        onCellClick: (e: Event, args: OnEventArgs) => {
          console.log(args);
          if (confirm('Are you sure?')) {
            this.practiceTextService.deletePracticeText(this.currentUserName, args.dataContext.id)
            .subscribe(response => {
              this.practiceTextList = response['result'];
              this.toastr.success('Text deleted successfully');
            }, error => this.toastr.error('Delete text failed, try again'));
          }
        }
      },
      { id: 'title', name: 'Title', field: 'title', sortable: true },
      { id: 'totalWords', name: 'Total Words', field: 'totalWords', sortable: true },
      { id: 'totalCharacters', name: 'Total Characters', field: 'totalCharacters', sortable: true },
      // { id: 'isPublic', name: 'Is Public', field: 'isPublic', formatter: Formatters.checkmark, sortable: true },
      { id: 'modifiedDate', name: 'Modified Date', field: 'modifiedDate', formatter: Formatters.dateIso, sortable: true }
    ];
    this.gridOptions = {
      enableAutoResize: true,
      enableCellNavigation: true,
      enablePagination: true,
      enableRowSelection: true,
      enableCheckboxSelector: true,
      rowSelectionOptions: {
        // True (Single Selection), False (Multiple Selections)
        selectActiveRow: true
      },
      checkboxSelector: {
        // remove the unnecessary "Select All" checkbox in header when in single selection mode
        hideSelectAllCheckbox: true
      },
    };
  }
}
