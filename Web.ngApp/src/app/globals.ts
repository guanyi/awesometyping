export class Globals {
    static baseUrlWebApi = 'https://localhost:44374/api';
    static emptyGuid = '00000000-0000-0000-0000-000000000000';
    static unassignedGuid = '00000000-0000-0000-0000-000000000001';
    static inactiveGuid = '00000000-0000-0000-0000-000000000002';
    static white = '#FFFFFF';
    static green = '#CCFF99';
    static red = '#FF9999';
    static defaultPracticeText = 'Load text to type';
    static checkedCharacters: string[] = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
     'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8',
      '9', ',', '.', ';', '?', '!', '\'', '"'];
}
