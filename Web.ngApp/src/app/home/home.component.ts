import { TypingResultDetail } from './../shared/models/typingResultDetail.interface';
import { Component, OnInit, Injectable, AfterViewInit, ViewChild, ElementRef, Renderer2 } from '@angular/core';
import { Globals } from '../globals';
import { map, tap } from 'rxjs/operators';
import { PracticeText, Characters, TypingText, TypingResult } from '../shared/models/index';
import { AuthorizeService } from '../../api-authorization/authorize.service';
import { TypingResultService, DataExchangeService } from '../shared/services/index';
import { ToastrService } from 'ngx-toastr';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

@Injectable()
export class HomeComponent implements OnInit, AfterViewInit {
  @ViewChild('typingArea') typingAreaElement: ElementRef;
  currentUserName: string;
  loadedPracticeText: PracticeText;
  loadedText: string;
  typingText: TypingText;
  startTime: number;
  typingTime: number;
  tempErrorRecords: string[];
  typingResult: TypingResult;
  typeErrorCount = 0;
  accuracy: number;
  wpm: number;

  constructor(private authorizeService: AuthorizeService
    , private typingResultService: TypingResultService
    , private dataExchangeService: DataExchangeService
    , private toastr: ToastrService) {
    this.loadedText = Globals.defaultPracticeText;
    this.loadText();
  }

  ngOnInit() {
    this.authorizeService.getUser()
    .pipe(map(u => u && u.name))
    .subscribe(user => this.currentUserName = user);
    this.dataExchangeService.loadedPracticeText.subscribe(practiceText => this.loadedPracticeText = practiceText);
    if (this.loadedPracticeText != null) {
      this.loadedText = this.loadedPracticeText.text;
    }
    this.loadText();
  }

  ngAfterViewInit() {
    setTimeout(() => this.typingAreaElement.nativeElement.focus());
  }

  loadText() {
    let character: string;
    const characters: Characters[] = [];
    let displayCharacter: string;
    for (let i = 0; i < this.loadedText.length; i++) {
      character = this.loadedText.charAt(i);
      if (character === ' ') {
        displayCharacter = '\xa0';
      } else {
        displayCharacter = character;
      }
      characters.push({ ch: character, displayedCh: displayCharacter, isTyped: false, TypeCorrect: false });
    }
    this.typingText = {characters: characters, currectTypingIndex: 0};
    this.typeErrorCount = 0;
  }

  getBackgroundColor(isTyped: boolean, TypeCorrect: boolean): string {
    if (isTyped && TypeCorrect) {
      return Globals.green;
    } else if (isTyped && !TypeCorrect) {
      return Globals.red;
    } else {
      return Globals.white;
    }
  }

  typeKey(event: any) {
    if (this.typingText.currectTypingIndex > this.typingText.characters.length) {
      return;
    }
    if (this.typingText.currectTypingIndex === 0) {
      this.startTime = Date.now();
      this.initTypingResult();
      this.tempErrorRecords = [];
    }
    const currentTypingChar = this.typingText.characters[this.typingText.currectTypingIndex];
    currentTypingChar.isTyped = true;
    if (event.key === currentTypingChar.ch) {
      currentTypingChar.TypeCorrect = true;
    } else {
      this.typeErrorCount++;
      this.tempErrorRecords.push(currentTypingChar.ch);
    }
    if (this.typingText.currectTypingIndex === this.typingText.characters.length - 1) {
      this.typingTime = Date.now() - this.startTime;
      this.generateTypingResult();
      this.typingResultService.createTypingResult(this.typingResult)
      .subscribe(practiceText => {
          this.toastr.success('Typing Result saved successfully');
      }, error => this.toastr.error('Typing Result save failed, try again'));
    }
    this.typingText.currectTypingIndex++;
  }

  generateTypingResult() {
    const totalCharacters = this.loadedPracticeText.totalCharacters;
    const minutes = this.typingTime / 1000 / 60;
    this.typingResult.durationSeconds = Math.round(this.typingTime / 1000);
    this.typingResult.wpm = (totalCharacters - this.typeErrorCount) / 5 / minutes;
    this.typingResult.mistakeCount = this.typeErrorCount;
    this.typingResult.accuracy = (totalCharacters - this.typeErrorCount) / totalCharacters;
    this.wpm =  this.typingResult.wpm;
    this.accuracy =  this.typingResult.accuracy;
    for (let i = 0; i < this.tempErrorRecords.length; i++) {
      for (let j = 0; j < this.typingResult.typingResultDetails.length; j++) {
        if (this.tempErrorRecords[i] === this.typingResult.typingResultDetails[j].TypedWrongCharacter) {
          this.typingResult.typingResultDetails[j].Occurrence++;
        }
      }
    }
  }

  initTypingResult() {
    const typingResult = {
      id: Globals.emptyGuid
      , PracticeTextId: this.loadedPracticeText.id
      , durationSeconds: 0
      , wpm: 0
      , mistakeCount: 0
      , accuracy: 0
      , userName: this.currentUserName
      , createDate: new Date()
      , typingResultDetails: []
    };

    for (let i = 0; i < Globals.checkedCharacters.length; i++) {
      const typingResultDetail = {
          id: Globals.emptyGuid
        , TypingResultId: Globals.emptyGuid
        , TypedWrongCharacter: Globals.checkedCharacters[i]
        , Occurrence: 0
        , mistakeRatio: 0 };
      typingResult.typingResultDetails.push(typingResultDetail);
    }
    this.typingResult = typingResult;
  }
}
