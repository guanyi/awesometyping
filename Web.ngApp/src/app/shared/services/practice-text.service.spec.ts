import { TestBed } from '@angular/core/testing';

import { PracticeTextService } from './practice-text.service';

describe('PracticeTextService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PracticeTextService = TestBed.get(PracticeTextService);
    expect(service).toBeTruthy();
  });
});
