import { Subscriber } from 'rxjs/Subscriber';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

export class DataService {

  constructor(protected http: HttpClient, protected url: string) {
  }

  create<T>(resource): Observable<T> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };
    return this.http.post<T>(this.url, resource, httpOptions);
  }

  delete (id: string | number, item: any): Observable<any> {
    const httpHeader = this.getDefaultHttpHeaders();
    httpHeader.observe = 'response';
    httpHeader.body = item;
    // Include the item in the body because the server side will examine whether the user has access to the business unit (contained in the item)
    return this.http.request('delete', `${this.url}/${id}`, httpHeader);
  }

  update<T>(resource): Observable<T> {
    return this.http.put<T>(`${this.url}/${resource.id}`, resource);
  }

  get<T>(id: string): Observable<T> {
    return this.http.get<T>(`${this.url}/${id}`);
  }

  getByLocale<T>(id: string, locale: string): Observable<T> {
    const params = new HttpParams().set('locale', locale);
    return this.http.get<T>(`${this.url}/${id}`, {params: params});
  }

  // the defined default http headers object (we always send these values in our requests)
  protected getDefaultHttpHeaders(): any {
    const test = new HttpHeaders();
    return {
      headers: new HttpHeaders(), // the default header information (blank by default)
    };
  }
}
