import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { Globals } from '../../globals';
import { DataService } from './data.service';
import { TypingResult } from '../models/index';

@Injectable({
  providedIn: 'root'
})
export class TypingResultService extends DataService {

  constructor(http: HttpClient) {
    super(http, `${Globals.baseUrlWebApi}/TypingResult`);
  }

  public createTypingResult(typingResult: TypingResult): Observable<any> {
    const httpHeader = this.getDefaultHttpHeaders();
    const url = `${Globals.baseUrlWebApi}/TypingResult`;
    return this.http.post<TypingResult>(url, typingResult, httpHeader);
  }
}
