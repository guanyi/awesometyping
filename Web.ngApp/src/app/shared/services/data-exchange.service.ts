import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Injectable } from '@angular/core';
import { PracticeText, Characters, TypingText, TypingResult } from '../models/index';

@Injectable({
  providedIn: 'root'
})
export class DataExchangeService {
  private practiceText = new BehaviorSubject<PracticeText>(null);
  loadedPracticeText = this.practiceText.asObservable();

  constructor() { }

  loadPracticeTextToType(practiceText: PracticeText) {
    this.practiceText.next(practiceText);
  }
}
