import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { Globals } from '../../globals';
import { DataService } from './data.service';
import { PracticeText } from '../models/index';

@Injectable({
  providedIn: 'root'
})
export class PracticeTextService extends DataService {

  constructor(http: HttpClient) {
    super(http, `${Globals.baseUrlWebApi}/PracticeText`);
  }

  getAllPracticeTextForAUser(currentUserName: string) {
    const url = `${Globals.baseUrlWebApi}/PracticeText/${currentUserName}/AllPracticeTexts`;
    return this.http.get(url);
  }
  getBestPracticeTextForAUser(currentUserName: string) {
    const url = `${Globals.baseUrlWebApi}/PracticeText/${currentUserName}/BestPracticeTexts`;
    return this.http.get(url);
  }

  getAPracticeTextForAUser(currentUserName: string, practiceTextId: string) {
    const url = `${Globals.baseUrlWebApi}/PracticeText/${currentUserName}/${practiceTextId}`;
    return this.http.get(url);
  }

  public createPracticeText(practiceText: PracticeText): Observable<any> {
    const httpHeader = this.getDefaultHttpHeaders();
    const url = `${Globals.baseUrlWebApi}/PracticeText`;
    return this.http.post<PracticeText>(url, practiceText, httpHeader);
  }

  public updatePracticeText(practiceText: PracticeText): Observable<any> {
    const httpHeader = this.getDefaultHttpHeaders();
    const url = `${Globals.baseUrlWebApi}/PracticeText/${practiceText.userName}/${practiceText.id}`;
    return this.http.put<PracticeText>(url, practiceText, httpHeader);
  }

  public deletePracticeText(currentUserName: string, practiceTextId: string) {
    const httpHeader = this.getDefaultHttpHeaders();
    const url = `${Globals.baseUrlWebApi}/PracticeText/${currentUserName}/${practiceTextId}`;
    return this.http.delete(url, httpHeader);
  }
}
