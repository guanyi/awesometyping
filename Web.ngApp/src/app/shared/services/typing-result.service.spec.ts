import { TestBed } from '@angular/core/testing';

import { TypingResultService } from './typing-result.service';

describe('TypingResultService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TypingResultService = TestBed.get(TypingResultService);
    expect(service).toBeTruthy();
  });
});
