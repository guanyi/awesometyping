import { TypingResultDetail } from './index';

export interface TypingResult {
    id: string;
    PracticeTextId: string;
    durationSeconds: number;
    mistakeCount: number;
    wpm: number;
    accuracy: number;
    userName: string;
    createDate: Date;
    typingResultDetails: TypingResultDetail[];
  }
