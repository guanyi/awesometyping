export interface TypingResultDetail {
    id: string;
    TypingResultId: string;
    TypedWrongCharacter: string;
    Occurrence: number;
    mistakeRatio: number;
  }
