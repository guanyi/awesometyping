export interface PracticeText {
  id: string;
  title: string;
  text: string;
  totalWords: number;
  totalCharacters: number;
  isPublic: boolean;
  userName: string;
  modifiedDate: Date;
}
