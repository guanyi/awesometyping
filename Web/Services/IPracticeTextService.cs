﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.ViewModels;

namespace Web.Services
{
    public interface IPracticeTextService
    {
        Task AddAsync(string currentUserId, PracticeTextVm practiceTextVm);
        Task<PracticeTextVm> GetAsync(Guid PracticeTextId);
        Task<ICollection<PracticeTextVm>> GetAllPracticeTextForAUserAsync(string currentUserId);
        Task<PracticeTextVm> GetAPracticeTextForAUserAsync(string currentUserId, Guid practiceTextId);
        Task<ICollection<PracticeTextVm>> GetBestPracticeTextForAUserAsync(string currentUserId, int number);
        Task UpdatePracticeTextAsync(string currentUserId, Guid practiceTextId, PracticeTextVm practiceTextVm);
        Task DeletePracticeTextAsync(string currentUserId, Guid practiceTextId);
        Task<bool> SaveAllAsync();
    }
}
