﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Threading.Tasks;
using Web.Models;
using Web.Repositories;

namespace Web.Services
{
    public class AuthenticationService : IAuthenticationService
    {
        private readonly IApplicationUserRepository _applicationUserRepository;        

        public AuthenticationService(IApplicationUserRepository applicationUserRepository)
        {
            _applicationUserRepository = applicationUserRepository;
        }

        public ApplicationUser GetCurrentLoginUserAsync(string userName)
        {
            var userList = _applicationUserRepository.GetAllUsers().ToList();
            return userList.FirstOrDefault(e => e.UserName.ToLower() == userName.ToLower());
        }
    }
}
