﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Web.Data;
using Web.Models;
using Web.Repositories;
using Web.ViewModels;

namespace Web.Services
{
    public class PracticeTextService : IPracticeTextService
    {
        private readonly int numberOfHighestMistakeRatioSumSelected = 3;
        private readonly ICheckedCharacterService _checkedCharacterService;
        private readonly IPracticeTextRepository _practiceTextRepository;
        private readonly ITypingResultRepository _typingResultRepository;
        private readonly IMapper _mapper;

        public PracticeTextService(ICheckedCharacterService checkedCharacterService
            , IPracticeTextRepository practiceTextRepository
            , ITypingResultRepository typingResultRepository
            , IMapper mapper)
        {
            _checkedCharacterService = checkedCharacterService;
            _practiceTextRepository = practiceTextRepository;
            _typingResultRepository = typingResultRepository;
            _mapper = mapper;
        }
        public async Task AddAsync(string currentUserId, PracticeTextVm practiceTextVm)
        {
            var practiceText = new PracticeText(
                practiceTextVm.Id
                , currentUserId
                , practiceTextVm.Title
                , practiceTextVm.Text
                , practiceTextVm.TotalWords
                , practiceTextVm.TotalCharacters
                , practiceTextVm.IsPublic
                , true);
            await _practiceTextRepository.AddAsync(practiceText);
            await _checkedCharacterService.CreateCheckedCharacterSetAsync(practiceText, currentUserId);
        }

        public async Task<bool> SaveAllAsync()
        {
            return await _practiceTextRepository.SaveAllAsync();
        }

        public async Task<PracticeTextVm> GetAsync(Guid practiceTextId)
        {
            var practiceText = await _practiceTextRepository.GetPracticeTextAsync(practiceTextId);
            if (practiceText == null)
            {
                return null;
            }
            var practiceTextVm = _mapper.Map<PracticeTextVm>(practiceText);
            return practiceTextVm;
        }

        public async Task<ICollection<PracticeTextVm>> GetAllPracticeTextForAUserAsync(string currentUserId)
        {
            var practiceTextList = await _practiceTextRepository.GetAllPracticeTextForAUserAsync(currentUserId);
            if (practiceTextList == null)
            {
                return null;
            }
            var practiceTextVmList = _mapper.Map<List<PracticeTextVm>>(practiceTextList);
            return practiceTextVmList;
        }

        public async Task<PracticeTextVm> GetAPracticeTextForAUserAsync(string currentUserId, Guid practiceTextId)
        {
            var practiceText = await _practiceTextRepository.GetAPracticeTextForAUserAsync(currentUserId, practiceTextId);
            if (practiceText == null)
            {
                return null;
            }
            var practiceTextVm = _mapper.Map<PracticeTextVm>(practiceText);
            return practiceTextVm;
        }

        public async Task<ICollection<PracticeTextVm>> GetBestPracticeTextForAUserAsync(string currentUserId, int number)
        {
            var typingResultDetailList = await GetAllTypingResultDetailOfAUserIntoOneListAsync(currentUserId);
            var top3MostTyedWrongCharacters = GetTopThreeMostTyedWrongCharacters(typingResultDetailList);
            //get top 3 practice text id which have hight "percentage" of the 3 characters found in previous steps
            var selectedPracticeTextVmList = new List<PracticeTextVm>();
            var practiceTextIdList = new List<Guid>();
            foreach (var character in top3MostTyedWrongCharacters)
            {
                var practiceTextId = await _checkedCharacterService.GetPracticeTextIdOfHighestPercentageCharacterAsync(character);
                var practiceText = await _practiceTextRepository.GetAPracticeTextForAUserAsync(currentUserId, practiceTextId);
                if (!practiceTextIdList.Contains(practiceTextId))
                {
                    selectedPracticeTextVmList.Add(_mapper.Map<PracticeTextVm>(practiceText));
                    practiceTextIdList.Add(practiceTextId);
                }                
            }
            return selectedPracticeTextVmList;
        }

        private async Task<List<TypingResultDetail>> GetAllTypingResultDetailOfAUserIntoOneListAsync(string currentUserId)
        {
            var typingResultList = await _typingResultRepository.GetAllTypingResultsForAUserAsync(currentUserId);
            if (typingResultList == null)
            {
                return null;
            }
            //combine all typing result details under each typing result into one list 
            var typingResultDetailList = new List<TypingResultDetail>();
            foreach (var typingResult in typingResultList)
            {
                typingResultDetailList.AddRange(typingResult.TypingResultDetails);
            }
            return typingResultDetailList;
        }

        private List<string> GetTopThreeMostTyedWrongCharacters(List<TypingResultDetail>typingResultDetailList)
        {
            //get top 3 characters which have highest type mistake ratio sum
            var typeWrongCharacterMistakeRatioList = new List<CharacterTypeMistakeRatio>();
            foreach (var character in Constants.CheckedCharacterTemplate)
            {
                var totalMistakeRatio = typingResultDetailList.Where(d => d.TypedWrongCharacter == character).Sum(d => d.MistakeRatio);
                typeWrongCharacterMistakeRatioList.Add(new CharacterTypeMistakeRatio(character, totalMistakeRatio));
            }
            List<string> top3MostTyedWrongCharacters = typeWrongCharacterMistakeRatioList.OrderByDescending(c => c.MistakeRatioSum)
                .Take(numberOfHighestMistakeRatioSumSelected)
                .Select(c => c.TypedWrongCharacter)
                .ToList();
            return top3MostTyedWrongCharacters;
        }

        public async Task UpdatePracticeTextAsync(string currentUserId, Guid practiceTextId, PracticeTextVm practiceTextVm)
        {
            var practiceTextToUpdate = await _practiceTextRepository.GetAPracticeTextForAUserAsync(currentUserId, practiceTextId);
            if (practiceTextToUpdate == null)
            {
                throw new KeyNotFoundException();
            }
            var practiceTextFromInput = _mapper.Map<PracticeText>(practiceTextVm);
            practiceTextToUpdate.Update(practiceTextFromInput, currentUserId);
            _practiceTextRepository.Update(practiceTextToUpdate);
            _checkedCharacterService.UpdateCheckedCharacterSet(practiceTextToUpdate);
        }

        public async Task DeletePracticeTextAsync(string currentUserId, Guid practiceTextId)
        {
            var practiceTextToDelete = await _practiceTextRepository.GetAPracticeTextForAUserAsync(currentUserId, practiceTextId);
            if (practiceTextToDelete == null)
            {
                throw new KeyNotFoundException();
            }
            practiceTextToDelete.Delete();
            _checkedCharacterService.DeleteCheckedCharacterSet(practiceTextToDelete.CheckedCharacters);
            _practiceTextRepository.Update(practiceTextToDelete);
        }
    }
}
