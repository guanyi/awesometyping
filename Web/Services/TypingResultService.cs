﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Models;
using Web.Repositories;
using Web.ViewModels;

namespace Web.Services
{
    public class TypingResultService : ITypingResultService
    {
        private readonly ITypingResultRepository _typingResultRepository;
        private readonly ITypingResultDetailRepository _typingResultDetailRepository;
        private readonly ICheckedCharacterRepository _checkedCharacterRepository;

        public TypingResultService(ITypingResultRepository typingResultRepository
            , ITypingResultDetailRepository typingResultDetailRepository
            , ICheckedCharacterRepository checkedCharacterRepository)
        {
            _typingResultRepository = typingResultRepository;
            _typingResultDetailRepository = typingResultDetailRepository;
            _checkedCharacterRepository = checkedCharacterRepository;
        }

        public async Task AddAsync(string currentUserId, TypingResultVm typingResultVm)
        {
            var typingResult = new TypingResult(
                Guid.NewGuid()
                , typingResultVm.PracticeTextId
                , typingResultVm.DurationSeconds
                , typingResultVm.MistakeCount
                , typingResultVm.Wpm
                , typingResultVm.Accuracy
                , currentUserId);
            await _typingResultRepository.AddAsync(typingResult);
            foreach (var typingResultDetailVm in typingResultVm.TypingResultDetails)
            {
                typingResult.TypingResultDetails.Add(new TypingResultDetail(
                      Guid.NewGuid()
                    , typingResultDetailVm.TypingResultId
                    , typingResultDetailVm.TypedWrongCharacter
                    , typingResultDetailVm.Occurrence
                    , typingResultDetailVm.MistakeRatio
                    , new Guid(currentUserId)));
            }
            var typedWrongTypingResultDetails = typingResult.TypingResultDetails.Where(e => e.Occurrence != 0);
            var checkedCharacters = await _checkedCharacterRepository.GetCheckedCharactersOfAPracticeTextAsync(typingResult.PracticeTextId);
            foreach (var typedWrongTypingResultDetail in typedWrongTypingResultDetails)
            {
                var percentage = checkedCharacters.FirstOrDefault(c => c.Character == typedWrongTypingResultDetail.TypedWrongCharacter).Percentage;
                typedWrongTypingResultDetail.UpdateMistakeRatio(typedWrongTypingResultDetail.Occurrence / percentage);
            }
            await _typingResultDetailRepository.AddRangeAsync(typingResult.TypingResultDetails);
        }

        public async Task<bool> SaveAllAsync()
        {
            return await _typingResultRepository.SaveAllAsync();
        }
    }
}
