﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Models;

namespace Web.Services
{
    public interface ICheckedCharacterService
    {
        Task<Guid> GetPracticeTextIdOfHighestPercentageCharacterAsync(string character);
        Task CreateCheckedCharacterSetAsync(PracticeText practiceText, string currentUser);
        void UpdateCheckedCharacterSet(PracticeText practiceText);
        void DeleteCheckedCharacterSet(ICollection<CheckedCharacter> checkedCharacters);
        Task<bool> SaveAllAsync();
    }
}
