﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Services.Startup
{
    public static class ServiceCollectionExtensionsService
    {
        public static IServiceCollection AddCustomizedServices(this IServiceCollection services)
        {
            // Add services to DI container here.
            services.AddScoped<IPracticeTextService, PracticeTextService>();
            services.AddScoped<IAuthenticationService, AuthenticationService>();
            services.AddScoped<ICheckedCharacterService, CheckedCharacterService>();
            services.AddScoped<ITypingResultService, TypingResultService>();
            return services;
        }
    }
}
