﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Repositories;

namespace Web.Services.Startup
{
    public static class ServiceCollectionExtensionsRepository
    {
        public static IServiceCollection AddCustomizedRepositories(this IServiceCollection services)
        {
            services.AddScoped<IPracticeTextRepository, PracticeTextRepository>();
            services.AddScoped<IApplicationUserRepository, ApplicationUserRepository>();
            services.AddScoped<ICheckedCharacterRepository, CheckedCharacterRepository>();
            services.AddScoped<ITypingResultRepository, TypingResultRepository>();
            services.AddScoped<ITypingResultDetailRepository, TypingResultDetailRepository>();
            return services;
        }
    }
}
