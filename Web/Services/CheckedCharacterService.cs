﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Models;
using Web.Repositories;

namespace Web.Services
{
    public class CheckedCharacterService: ICheckedCharacterService
    {
        private readonly ICheckedCharacterRepository _checkedCharacterRepository;

        public CheckedCharacterService(ICheckedCharacterRepository checkedCharacterRepository)
        {
            _checkedCharacterRepository = checkedCharacterRepository;
        }

        public async Task<Guid> GetPracticeTextIdOfHighestPercentageCharacterAsync(string character)
        {
            return await _checkedCharacterRepository.GetPracticeTextIdOfHighestPercentageCharacterAsync(character);
        }

        public async Task CreateCheckedCharacterSetAsync(PracticeText practiceText, string currentUserId)
        {
            var text = practiceText.Text;
            var totalCharacters = practiceText.TotalCharacters;
            List<CheckedCharacter> checkedCharacterList = new List<CheckedCharacter>();
            foreach (var character in Constants.CheckedCharacterTemplate)
            {
                var percentage = (double)(text.Split(character).Length - 1) / totalCharacters;
                var newCharacter = new CheckedCharacter(
                Guid.NewGuid()
                , practiceText.Id
                , character
                , percentage
                , true
                , currentUserId);
                checkedCharacterList.Add(newCharacter);
            }
            await _checkedCharacterRepository.AddRangeAsync(checkedCharacterList);
        }

        public void UpdateCheckedCharacterSet(PracticeText practiceText)
        {
            var text = practiceText.Text;
            var totalCharacters = practiceText.TotalCharacters;
            foreach (var character in Constants.CheckedCharacterTemplate)
            {
                var percentage = (double)(text.Split(character).Length - 1) / totalCharacters;
                practiceText.CheckedCharacters.FirstOrDefault(c => c.Character == character)?.UpdatePercentage(percentage);
            }
            _checkedCharacterRepository.UpdateRange(practiceText.CheckedCharacters);
        }

        public void DeleteCheckedCharacterSet(ICollection<CheckedCharacter> checkedCharacters)
        {
            var checkedCharacterList = checkedCharacters.ToList();
            checkedCharacterList.ForEach(e => e.DeleteCheckedCharacter());
            _checkedCharacterRepository.UpdateRange(checkedCharacterList);
        }

        public async Task<bool> SaveAllAsync()
        {
            return await _checkedCharacterRepository.SaveAllAsync();
        }
    }
}
