﻿using System.Security.Principal;
using System.Threading.Tasks;
using Web.Models;

namespace Web.Services
{
    public interface IAuthenticationService
    {
        ApplicationUser GetCurrentLoginUserAsync(string userName);
    }
}
