﻿using Web.Models;
using IdentityServer4.EntityFramework.Options;
using Microsoft.AspNetCore.ApiAuthorization.IdentityServer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Web.Data
{
    public class ApplicationDbContext : ApiAuthorizationDbContext<ApplicationUser>
    {
        private bool _hasChanges = false;
        public ApplicationDbContext(DbContextOptions options, IOptions<OperationalStoreOptions> operationalStoreOptions)
            : base(options, operationalStoreOptions)
        {
        }

        public DbSet<PracticeText> PracticeTexts { get; set; }
        public DbSet<CheckedCharacter> CheckedCharacters { get; set; }
        public DbSet<TypingResult> TypingResults { get; set; }
        public DbSet<TypingResultDetail> TypingResultDetails { get; set; }

        public virtual async Task<bool> SaveChangesAsync()
        {
            ApplyStateChanges();
            if (!_hasChanges) return true;
            try
            {
                if (await base.SaveChangesAsync() > 0)
                {
                    Refresh();
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return false;
        }

        public new void SaveChanges()
        {
            base.SaveChanges();
        }

        protected void ApplyStateChanges()
        {
            var changeTrackerEntries = ChangeTracker.Entries()
                .Where(e => e.Entity is Entity<Guid>);
            foreach (var entry in changeTrackerEntries)
            {
                var stateInfo = (Entity<Guid>)entry.Entity;
                entry.State = ConvertState(stateInfo.State);
                _hasChanges = true;
            }
#if DEBUG
            //DisplayTrackedEntities(ChangeTracker);
#endif
        }
        private void Refresh()
        {
            var entities = this.ChangeTracker
                    .Entries()
                    .Where(e => e.Entity is Entity<Guid>)
                    .Select(e => e.Entity)
                    .ToList();
            foreach (var entity in entities)
            {
                ((Entity<Guid>)entity).State = State.Unchanged;
            }
        }

        private static EntityState ConvertState(State state)
        {
            switch (state)
            {
                case State.Added:
                    return EntityState.Added;
                case State.Modified:
                    return EntityState.Modified;
                case State.Deleted:
                    return EntityState.Deleted;
                default:
                    return EntityState.Unchanged;
            }
        }
    }
}
