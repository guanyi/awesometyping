﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Web.Data.Migrations
{
    public partial class CreateTypingRelatedTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PracticeTexts",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    CreatedBy = table.Column<Guid>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<Guid>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    TimeStamp = table.Column<byte[]>(rowVersion: true, nullable: true),
                    UserId = table.Column<string>(nullable: false),
                    Title = table.Column<string>(nullable: false),
                    Text = table.Column<string>(nullable: false),
                    TotalWords = table.Column<int>(nullable: false),
                    TotalCharacters = table.Column<int>(nullable: false),
                    IsPublic = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PracticeTexts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PracticeTexts_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CheckedCharacters",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    CreatedBy = table.Column<Guid>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<Guid>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    TimeStamp = table.Column<byte[]>(rowVersion: true, nullable: true),
                    PracticeTextId = table.Column<Guid>(nullable: false),
                    Character = table.Column<string>(nullable: false),
                    Percentage = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CheckedCharacters", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CheckedCharacters_PracticeTexts_PracticeTextId",
                        column: x => x.PracticeTextId,
                        principalTable: "PracticeTexts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TypingResults",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    CreatedBy = table.Column<Guid>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<Guid>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    TimeStamp = table.Column<byte[]>(rowVersion: true, nullable: true),
                    PracticeTextId = table.Column<Guid>(nullable: false),
                    DurationSeconds = table.Column<int>(nullable: false),
                    MistakeCount = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TypingResults", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TypingResults_PracticeTexts_PracticeTextId",
                        column: x => x.PracticeTextId,
                        principalTable: "PracticeTexts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TypingResultDetails",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    CreatedBy = table.Column<Guid>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<Guid>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    TimeStamp = table.Column<byte[]>(rowVersion: true, nullable: true),
                    TypingResultId = table.Column<Guid>(nullable: false),
                    TypedWrongCharacter = table.Column<string>(nullable: false),
                    Occurrence = table.Column<double>(nullable: false),
                    MistakeRatio = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TypingResultDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TypingResultDetails_TypingResults_TypingResultId",
                        column: x => x.TypingResultId,
                        principalTable: "TypingResults",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CheckedCharacters_PracticeTextId",
                table: "CheckedCharacters",
                column: "PracticeTextId");

            migrationBuilder.CreateIndex(
                name: "IX_PracticeTexts_UserId",
                table: "PracticeTexts",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_TypingResultDetails_TypingResultId",
                table: "TypingResultDetails",
                column: "TypingResultId");

            migrationBuilder.CreateIndex(
                name: "IX_TypingResults_PracticeTextId",
                table: "TypingResults",
                column: "PracticeTextId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CheckedCharacters");

            migrationBuilder.DropTable(
                name: "TypingResultDetails");

            migrationBuilder.DropTable(
                name: "TypingResults");

            migrationBuilder.DropTable(
                name: "PracticeTexts");
        }
    }
}
