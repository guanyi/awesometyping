﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Web.Models;
using Web.Services;
using Web.ViewModels;

namespace Web.Controllers
{
    [Route("api/[controller]")]
    public class PracticeTextController : Controller
    {
        private IPracticeTextService _practiceTextService;
        private readonly IAuthenticationService _authenticationService;

        public PracticeTextController(IPracticeTextService practiceTextService, IAuthenticationService authenticationService)
        {
            _practiceTextService = practiceTextService;
            _authenticationService = authenticationService;
        }

        [HttpGet("{currentUserName}/AllPracticeTexts")]
        public async Task<IActionResult> GetAllPracticeTextForAUserAsync(string currentUserName)
        {
            try
            {
                var currentUser = _authenticationService.GetCurrentLoginUserAsync(currentUserName);
                if (currentUser == null)
                {
                    return BadRequest(Constants.Exception.Common.ValidationUserNameRequired);
                }
                var practiceTextList = _practiceTextService.GetAllPracticeTextForAUserAsync(currentUser.Id);
                return Ok(practiceTextList);
            }
            catch (Exception ex)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, Constants.Exception.Common.InternalServerError);
            }
        }

        [HttpGet("{currentUserName}/{practiceTextId}")]
        public async Task<IActionResult> GetAPracticeTextForAUserAsync(string currentUserName, string practiceTextId)
        {
            try
            {
                var currentUser = _authenticationService.GetCurrentLoginUserAsync(currentUserName);
                if (currentUser == null)
                {
                    return BadRequest(Constants.Exception.Common.ValidationUserNameRequired);
                }
                var practiceText = await _practiceTextService.GetAPracticeTextForAUserAsync(currentUser.Id, new Guid(practiceTextId));
                if (practiceText == null)
                {
                    return NotFound(Constants.Exception.PracticeTextError.NotFound);
                }
                return Ok(practiceText);
            }
            catch (Exception ex)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, Constants.Exception.Common.InternalServerError);
            }
        }

        [HttpGet("{currentUserName}/BestPracticeTexts")]
        public async Task<IActionResult> GetBestPracticeTextForAUserAsync(string currentUserName, int number = 3)
        {
            try
            {
                var currentUser = _authenticationService.GetCurrentLoginUserAsync(currentUserName);
                if (currentUser == null)
                {
                    return BadRequest(Constants.Exception.Common.ValidationUserNameRequired);
                }
                var practiceTextList = _practiceTextService.GetBestPracticeTextForAUserAsync(currentUser.Id, number);
                return Ok(practiceTextList);
            }
            catch (Exception ex)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, Constants.Exception.Common.InternalServerError);
            }
        }

        [HttpPost]
        public async Task<IActionResult> PostPracticeTextAsync([FromBody] PracticeTextVm practiceTextVm)
        {
            try
            {
                var currentUser = _authenticationService.GetCurrentLoginUserAsync(practiceTextVm.UserName);
                if (currentUser == null)
                {
                    return BadRequest(Constants.Exception.Common.ValidationUserNameRequired);
                }
                practiceTextVm.Id = Guid.NewGuid();
                await _practiceTextService.AddAsync(currentUser.Id, practiceTextVm);
                if (await _practiceTextService.SaveAllAsync())
                {
                    var newPracticeText = await _practiceTextService.GetAsync(practiceTextVm.Id);
                    return Ok(newPracticeText);
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException.Message.Contains(Constants.Exception.Common.MessageDuplicateKey))
                {
                    return BadRequest(Constants.Exception.PracticeTextError.BadRequestCreateDuplicateKey);
                }
            }
            return StatusCode((int)HttpStatusCode.InternalServerError, Constants.Exception.Common.InternalServerError);
        }

        [HttpPut("{currentUserName}/{practiceTextId}")]
        public async Task<IActionResult> UpdatePracticeTextAsync(string currentUserName, string practiceTextId, [FromBody] PracticeTextVm practiceTextVm)
        {
            try
            {
                var currentUser = _authenticationService.GetCurrentLoginUserAsync(currentUserName);
                if (currentUser == null)
                {
                    return BadRequest(Constants.Exception.Common.ValidationUserNameRequired);
                }

                var oldPracticeText = await _practiceTextService.GetAPracticeTextForAUserAsync(currentUser.Id, new Guid(practiceTextId));

                if (oldPracticeText == null)
                {
                    return NotFound(Constants.Exception.PracticeTextError.NotFound);
                }

                await _practiceTextService.UpdatePracticeTextAsync(currentUser.Id, new Guid(practiceTextId), practiceTextVm);
                if (await _practiceTextService.SaveAllAsync())
                {
                    var practiceText = await _practiceTextService.GetAPracticeTextForAUserAsync(currentUser.Id, new Guid(practiceTextId));
                    return Ok(practiceText);
                }
            }
            catch (Exception ex)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, Constants.Exception.Common.InternalServerError);
            }
            return StatusCode((int)HttpStatusCode.InternalServerError, Constants.Exception.Common.InternalServerError);
        }

        [HttpDelete("{currentUserName}/{practiceTextId}")]
        public async Task<IActionResult> DeletePracticeTextAsync(string currentUserName, string practiceTextId)
        {
            try
            {
                var currentUser = _authenticationService.GetCurrentLoginUserAsync(currentUserName);
                if (currentUser == null)
                {
                    return BadRequest(Constants.Exception.Common.ValidationUserNameRequired);
                }

                await _practiceTextService.DeletePracticeTextAsync(currentUser.Id, new Guid(practiceTextId));
                if (await _practiceTextService.SaveAllAsync())
                {
                    var practiceTextList = _practiceTextService.GetAllPracticeTextForAUserAsync(currentUser.Id);
                    return Ok(practiceTextList);
                }
            }
            catch (Exception ex)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, Constants.Exception.Common.InternalServerError);
            }
            return StatusCode((int)HttpStatusCode.InternalServerError, Constants.Exception.Common.InternalServerError);
        }
    }
}
