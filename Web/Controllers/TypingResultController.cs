﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Web.Models;
using Web.Services;
using Web.ViewModels;

namespace Web.Controllers
{
    [Route("api/[controller]")]
    public class TypingResultController : Controller
    {
        private ITypingResultService _typingResultService;
        private readonly IAuthenticationService _authenticationService;

        public TypingResultController(ITypingResultService typingResultService, IAuthenticationService authenticationService)
        {
            _typingResultService = typingResultService;
            _authenticationService = authenticationService;
        }

        [HttpPost]
        public async Task<IActionResult> PostTypingResultAsync([FromBody] TypingResultVm typingResultVm)
        {
            try
            {
                var currentUser = _authenticationService.GetCurrentLoginUserAsync(typingResultVm.UserName);
                if (currentUser == null)
                {
                    return BadRequest(Constants.Exception.Common.ValidationUserNameRequired);
                }
                await _typingResultService.AddAsync(currentUser.Id, typingResultVm);
                if (await _typingResultService.SaveAllAsync())
                {
                    return Ok();
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException.Message.Contains(Constants.Exception.Common.MessageDuplicateKey))
                {
                    return BadRequest(Constants.Exception.TypingResultError.BadRequestCreateDuplicateKey);
                }
            }
            return StatusCode((int)HttpStatusCode.InternalServerError, Constants.Exception.Common.InternalServerError);
        }
    }
}
