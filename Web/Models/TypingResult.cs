﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models
{
    public class TypingResult : Entity<Guid>
    {
        public TypingResult() { }
        public TypingResult(Guid id, Guid practiceTextId, int durationSeconds, int mistakeCount, double wpm, double accuracy, string userId) : base(id)
        {
            ApplicationUserId = userId;
            PracticeTextId = practiceTextId;
            DurationSeconds = durationSeconds;
            MistakeCount = mistakeCount;
            Wpm = wpm;
            Accuracy = accuracy;
            TypingResultDetails = new List<TypingResultDetail>();
            IsActive = true;
            CreatedBy = new Guid(userId); ;
            CreateDate = DateTime.UtcNow;
            ModifiedBy = new Guid(userId); ;
            ModifiedDate = DateTime.UtcNow;
            State = State.Added;
        }

        [Required]
        public string ApplicationUserId { get; set; }
        [Required]
        public Guid PracticeTextId { get; private set; }
        [Required]
        public int DurationSeconds { get; private set; }
        [Required]
        public int MistakeCount { get; private set; }
        [Required]
        public double Wpm { get; private set; }
        [Required]
        public double Accuracy { get; private set; }
        public ICollection<TypingResultDetail> TypingResultDetails { get; set; }
        public virtual PracticeText PracticeText { get; set; }
    }
}
