﻿namespace Web.Models
{
    public enum State
    {
        Unchanged,
        Added,
        Modified,
        Deleted
    }
}
