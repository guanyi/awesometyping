﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Web.Models;

namespace Web.Models
{
    public abstract class Entity<TId> : IEquatable<Entity<TId>>, ICloneable
    {
        public TId Id { get; set; }

        public System.DateTime CreateDate { get; set; }

        public Guid CreatedBy { get; set; }

        public System.DateTime ModifiedDate { get; set; }
        public Guid ModifiedBy { get; set; }
        public bool IsActive { get; set; }
        [NotMapped]
        public State State { get; set; }
        [Timestamp()]
        public byte[] TimeStamp { get; set; }
        public Entity(TId id)
        {
            if (object.Equals(id, default(TId)))
            {
                throw new ArgumentException("The ID cannot be the type's default value.", nameof(id));
            }

            this.Id = id;
            CreateDate = DateTime.UtcNow;
            CreatedBy = Constants.Id.Common.Na;
        }

        // EF requires an empty constructor
        protected Entity()
        {
        }

        // For simple entities, this may suffice
        // As Evans notes earlier in the course, equality of Entities is frequently not a simple operation
        public override bool Equals(object otherObject)
        {
            var entity = otherObject as Entity<TId>;
            return entity != null ? Equals(entity) : base.Equals(otherObject);
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public bool Equals(Entity<TId> other)
        {
            return other != null && this.Id.Equals(other.Id);
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }

    }
}
