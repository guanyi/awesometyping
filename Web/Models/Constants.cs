﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models
{
    public class Constants
    {
        public static readonly string[] CheckedCharacterTemplate = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l"
                , "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"
                , "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"
                , ",", ".", ";", "?", "!", "'", "\""};

        public class Exception
        {
            public class Common
            {
                public const string Angular = "ANGULAR_ERROR";
                public const string AppSettings = "ERROR.APP_SETTINGS";
                public const string BadRequestTokenCreate = "ERROR.BAD_REQUEST_TOKEN_CREATE";
                public const string InternalServerError = "ERROR.500_MESSAGE";
                public const string UnAuthorized = "ERROR.403_MESSAGE";
                public const string ValidationCodeRequired = "ERROR.CODE_IS_REQUIRED";
                public const string ValidationIdTokenRequired = "ERROR.ID_TOKEN_IS_REQUIRED";
                public const string ValidationUserNameRequired = "ERROR.USER_NAME_IS_REQUIRED";
                public static readonly string MessageDuplicateKey = "DUPLICATE_KEY";
            }

            public class PracticeTextError
            {
                public const string NotFound = "ERROR.PRACTICE_TEXT_NOT_FOUND";
                public const string BadRequestCreateDuplicateKey = "ERROR.PRACTICE_TEXT_DUPLICATE_KEY";
                public const string BadRequestGet = "ERROR.PRACTICE_TEXT_BAD_REQUEST_GET";
            }

            public class TypingResultError
            {
                public const string BadRequestCreateDuplicateKey = "ERROR.TYPING_RESULT_DUPLICATE_KEY";
                public const string BadRequestGet = "ERROR.TYPING_RESULT_BAD_REQUEST_POST";
            }

        }

        public class Name
        {
            public class Common
            {
                public const string DebugEnvironment = "Debug";
                public const string LocalEnvironment = "Local";
                public const string UrlHelper = "URLHELPER";
                public static readonly string Unassigned = "unassigned";
                public static readonly string UnassignedShort = "unasgd";
                public static readonly string Na = "n/a";
                public static readonly string MessageDuplicateKey = "duplicate key";
            }

            public class DomainEvent
            {
                public const string UpdateAction = "Update";
                public const string AddAction = "Add";
                public const string DeleteAction = "Delete";
            }
        }

        public class Id
        {
            public class Common
            {
                public static readonly Guid Empty = new Guid("00000000-0000-0000-0000-000000000000");
                public static readonly Guid Unassigned = new Guid("00000000-0000-0000-0000-000000000001");
                public static readonly Guid Na = new Guid("00000000-0000-0000-0000-000000000002");
            }

            public class Stage
            {
                public static readonly Guid Na = new Guid("00000000-0000-0000-0000-000000000002");
            }
        }
    }
}
