﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Web.ViewModels;

namespace Web.Models
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<PracticeText, PracticeTextVm>().ReverseMap();
            CreateMap<TypingResult, TypingResultVm>().ReverseMap();
            CreateMap<TypingResultDetail, TypingResultDetailVm>().ReverseMap();
        }
    }
}
