﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models
{
    public class PracticeText : Entity<Guid>
    {
        public PracticeText() { }
        public PracticeText(Guid id, string userId, string title, string text, int totalWords, int totalCharacters, bool isPublic, bool isActive) : base(id)
        {
            UserId = userId;
            Title = title;
            Text = text;
            TotalWords = totalWords;
            TotalCharacters = totalCharacters;
            IsPublic = isPublic;
            TypingResults = new List<TypingResult>();
            CheckedCharacters = new List<CheckedCharacter>();
            IsActive = isActive;
            CreatedBy = new Guid(userId);
            CreateDate = DateTime.UtcNow;
            ModifiedBy = new Guid(userId);
            ModifiedDate = DateTime.UtcNow;
            State = State.Added;
        }

        public void Update(PracticeText practiceText, string updateUserId)
        {
            Title = practiceText.Title;
            Text = practiceText.Text;
            TotalWords = practiceText.TotalWords;
            TotalCharacters = practiceText.TotalCharacters;
            ModifiedBy = new Guid(updateUserId);
            ModifiedDate = DateTime.UtcNow;
            State = State.Modified;
        }

        public void Delete()
        {
            IsActive = false;
            State = State.Modified;
        }

        [Required]
        public string UserId { get; set; }
        [Required]
        public string Title { get; private set; }
        [Required]
        public string Text { get; private set; }
        public int TotalWords { get; private set; }
        public int TotalCharacters { get; private set; }
        public bool IsPublic { get; private set; }
        public ICollection<TypingResult> TypingResults { get; set; }
        public ICollection<CheckedCharacter> CheckedCharacters { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}
