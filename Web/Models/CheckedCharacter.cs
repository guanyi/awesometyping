﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models
{
    public class CheckedCharacter : Entity<Guid>
    {
        public CheckedCharacter() { }
        public CheckedCharacter(Guid id, Guid practiceTextId, string character, double percentage, bool isActive, string updateUserId) : base(id)
        {
            PracticeTextId = practiceTextId;
            Character = character;
            Percentage = percentage;
            IsActive = isActive;
            CreatedBy = new Guid(updateUserId);
            CreateDate = DateTime.UtcNow;
            ModifiedBy = new Guid(updateUserId);
            ModifiedDate = DateTime.UtcNow;
            State = State.Added;
        }

        public void UpdatePercentage(double percentage)
        {
            Percentage = percentage;
            State = State.Modified;
        }

        public void DeleteCheckedCharacter()
        {
            IsActive = false;
            State = State.Modified;
        }

        public Guid PracticeTextId { get; private set; }
        [Required]
        public string Character { get; private set; }
        public double Percentage { get; private set; }
        public virtual PracticeText PracticeText { get; set; }
    }
}
