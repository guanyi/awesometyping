﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models
{
    public class TypingResultDetail : Entity<Guid>
    {
        public TypingResultDetail() { }
        public TypingResultDetail(Guid id, Guid typingResultId, string typedWrongCharacter, int occurrence, double mistakeRatio, Guid applicationUserId) : base(id)
        {
            TypingResultId = typingResultId;
            TypedWrongCharacter = typedWrongCharacter;
            Occurrence = occurrence;
            MistakeRatio = mistakeRatio;
            IsActive = true;
            CreatedBy = applicationUserId;
            CreateDate = DateTime.UtcNow;
            ModifiedBy = applicationUserId;
            ModifiedDate = DateTime.UtcNow;
            State = State.Added;
        }

        public void UpdateMistakeRatio(double mistakeRatio)
        {
            MistakeRatio = mistakeRatio;
        }

        public Guid TypingResultId { get; private set; }
        [Required]
        public string TypedWrongCharacter { get; private set; }
        public int Occurrence { get; private set; }
        public double MistakeRatio { get; private set; }
        public virtual TypingResult TypingResult { get; set; }
    }
}
