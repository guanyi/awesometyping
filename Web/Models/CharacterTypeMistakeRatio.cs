﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models
{
    public class CharacterTypeMistakeRatio
    {
        public string TypedWrongCharacter { get; set; }
        public double MistakeRatioSum { get; set; }

        public CharacterTypeMistakeRatio(string typedWrongCharacter, double mistakeRatioSum)
        {
            TypedWrongCharacter = typedWrongCharacter;
            MistakeRatioSum = mistakeRatioSum;
        }
    }
}
