﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.ViewModels
{
    public class TypingResultDetailVm
    {
        public Guid Id { get; set; }
        public Guid TypingResultId { get; set; }
        public string TypedWrongCharacter { get; set; }
        public int Occurrence { get; set; }
        public double MistakeRatio { get; set; }
    }
}
