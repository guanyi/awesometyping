﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.ViewModels
{
    public class TypingResultVm
    {
        public Guid Id { get; set; }
        public Guid PracticeTextId { get; set; }
        public int DurationSeconds { get; set; }
        public int MistakeCount { get; set; }
        public double Wpm { get; set; }
        public double Accuracy { get; set; }
        public string UserName { get; set; }
        public DateTime CreateDate { get; set; }
        public ICollection<TypingResultDetailVm> TypingResultDetails { get; set; }
    }
}
