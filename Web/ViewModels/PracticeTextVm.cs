﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.ViewModels
{
    public class PracticeTextVm
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public int TotalWords { get; set; }
        public int TotalCharacters { get; set; }
        public bool IsPublic { get; set; }
        public string UserName { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}
