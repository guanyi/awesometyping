﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Data;
using Web.Models;

namespace Web.Repositories
{
    public class TypingResultDetailRepository : GenericRepository<TypingResultDetail>, ITypingResultDetailRepository
    {
        private readonly ApplicationDbContext _applicationDbContext;

        public TypingResultDetailRepository(ApplicationDbContext applicationDbContext) : base(applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }

        public async Task<bool> SaveAllAsync()
        {
            return await _applicationDbContext.SaveChangesAsync();
        }
    }
}
