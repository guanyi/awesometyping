﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Data;
using Web.Models;

namespace Web.Repositories
{
    public class TypingResultRepository : GenericRepository<TypingResult>, ITypingResultRepository
    {
        private readonly ApplicationDbContext _applicationDbContext;

        public TypingResultRepository(ApplicationDbContext applicationDbContext) : base(applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }

        public async Task<IEnumerable<TypingResult>> GetAllTypingResultsForAUserAsync(string currentUserId)
        {
            return await _applicationDbContext.TypingResults
                .Include(e => e.TypingResultDetails)
                .AsNoTracking()
                .Where(e => e.ApplicationUserId == currentUserId && e.IsActive)
                .ToListAsync();
        }

        public async Task<bool> SaveAllAsync()
        {
            return await _applicationDbContext.SaveChangesAsync();
        }
    }
}
