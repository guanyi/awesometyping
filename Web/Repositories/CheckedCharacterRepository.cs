﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Data;
using Web.Models;

namespace Web.Repositories
{
    public class CheckedCharacterRepository : GenericRepository<CheckedCharacter>, ICheckedCharacterRepository
    {
        private readonly ApplicationDbContext _applicationDbContext;

        public CheckedCharacterRepository(ApplicationDbContext applicationDbContext) : base(applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }

        public async Task<List<CheckedCharacter>> GetCheckedCharactersOfAPracticeTextAsync(Guid practiceTextId)
        {
            return await _applicationDbContext.CheckedCharacters
                .AsNoTracking()
                .Where(e => e.PracticeTextId == practiceTextId)
                .ToListAsync();
        }

        public async Task<Guid> GetPracticeTextIdOfHighestPercentageCharacterAsync(string character)
        {
            var checkedCharacterRecord = await _applicationDbContext.CheckedCharacters
                .AsNoTracking()
                .Where(e => e.Character == character && e.IsActive == true)
                .OrderByDescending(e => e.Percentage)
                .FirstOrDefaultAsync();
            return checkedCharacterRecord.PracticeTextId;
        }

        public async Task<bool> SaveAllAsync()
        {
            return await _applicationDbContext.SaveChangesAsync();
        }
    }
}
