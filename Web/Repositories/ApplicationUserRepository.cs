﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Data;
using Web.Models;

namespace Web.Repositories
{
    public class ApplicationUserRepository : IApplicationUserRepository
    {
        private readonly ApplicationDbContext _applicationDbContext;

        public ApplicationUserRepository(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }
        public IQueryable<ApplicationUser> GetAllUsers()
        {
            return _applicationDbContext.Users
                .AsNoTracking();
        }

        public async Task<ApplicationUser> GetUserAsync(Guid id)
        {
            return await _applicationDbContext.Users
                .FirstOrDefaultAsync(e => e.Id == id.ToString());
        }

        public async Task<bool> SaveAllAsync()
        {
            return await _applicationDbContext.SaveChangesAsync();
        }
    }
}
