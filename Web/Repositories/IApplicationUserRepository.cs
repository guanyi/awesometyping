﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Models;

namespace Web.Repositories
{
    public interface IApplicationUserRepository
    {
        IQueryable<ApplicationUser> GetAllUsers();
        Task<ApplicationUser> GetUserAsync(Guid id);
        Task<bool> SaveAllAsync();
    }
}
