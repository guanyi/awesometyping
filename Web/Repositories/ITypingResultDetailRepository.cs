﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Models;

namespace Web.Repositories
{
    public interface ITypingResultDetailRepository
    {
        Task AddRangeAsync(IEnumerable<TypingResultDetail> typingResultDetails);
        Task<bool> SaveAllAsync();
    }
}
