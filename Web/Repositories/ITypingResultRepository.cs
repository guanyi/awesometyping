﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Models;

namespace Web.Repositories
{
    public interface ITypingResultRepository
    {
        Task<IEnumerable<TypingResult>> GetAllTypingResultsForAUserAsync(string currentUserId);
        Task AddAsync(TypingResult typingResult);
        Task<bool> SaveAllAsync();
    }
}
