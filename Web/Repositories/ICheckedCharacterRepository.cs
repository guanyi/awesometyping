﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Models;

namespace Web.Repositories
{
    public interface ICheckedCharacterRepository
    {
        Task<List<CheckedCharacter>> GetCheckedCharactersOfAPracticeTextAsync(Guid practiceTextId);
        Task<Guid> GetPracticeTextIdOfHighestPercentageCharacterAsync(string character);
        Task AddRangeAsync(IEnumerable<CheckedCharacter> checkedCharacterList);
        void UpdateRange(IEnumerable<CheckedCharacter> checkedCharacterList);
        Task<bool> SaveAllAsync();
    }
}
