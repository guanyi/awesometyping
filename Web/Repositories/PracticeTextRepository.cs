﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Data;
using Web.Models;

namespace Web.Repositories
{
    public class PracticeTextRepository : GenericRepository<PracticeText>, IPracticeTextRepository
    {
        private readonly ApplicationDbContext _applicationDbContext;

        public PracticeTextRepository(ApplicationDbContext applicationDbContext) : base(applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }

        public async Task<PracticeText> GetPracticeTextAsync(Guid id)
        {
            return await _applicationDbContext.PracticeTexts
                .AsNoTracking()
                .FirstOrDefaultAsync(e => e.Id == id);
        }

        public async Task<IEnumerable<PracticeText>> GetAllPracticeTextForAUserAsync(string currentUserId)
        {
            return await _applicationDbContext.PracticeTexts
                .AsNoTracking()
                .Where(e => e.UserId == currentUserId && e.IsActive)
                .ToListAsync();
        }

        public async Task<PracticeText> GetAPracticeTextForAUserAsync(string currentUserId, Guid practiceTextId)
        {
            return await _applicationDbContext.PracticeTexts
                .Include(e => e.CheckedCharacters)
                .AsNoTracking()
                .FirstOrDefaultAsync(e => e.UserId == currentUserId && e.Id == practiceTextId && e.IsActive);
        }

        public async Task<bool> SaveAllAsync()
        {
            return await _applicationDbContext.SaveChangesAsync();
        }
    }
}
