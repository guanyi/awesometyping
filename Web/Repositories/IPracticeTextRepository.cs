﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Models;

namespace Web.Repositories
{
    public interface IPracticeTextRepository
    {        
        Task AddAsync(PracticeText practiceText);
        Task<PracticeText> GetPracticeTextAsync(Guid id);
        Task<IEnumerable<PracticeText>> GetAllPracticeTextForAUserAsync(string currentUserId);
        Task<PracticeText> GetAPracticeTextForAUserAsync(string currentUserId, Guid practiceTextId);
        void Update(PracticeText practiceText); //Generic Repo Method
        Task<bool> SaveAllAsync();
    }
}
