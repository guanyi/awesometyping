﻿using AutoMapper;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Controllers;
using Web.Models;
using Web.Services;
using Web.ViewModels;

namespace Web.Test.Controllers
{
    [TestClass]
    public class TypingResultControllerTest
    {
        private TypingResultController _sutTypingResultController;
        private Mock<ITypingResultService> _mockTypingResultService;
        private Mock<IAuthenticationService> _mockAuthenticationService;

        private TypingResultVm _typingResultVm;
        private List<ApplicationUser> _userList;
        public TypingResultControllerTest()
        {
            _mockTypingResultService = new Mock<ITypingResultService>();
            _mockAuthenticationService = new Mock<IAuthenticationService>();
            AutoMapperInit();
        }

        [TestMethod]
        public async Task PostTypingResultAsync_ValidTypingResult_ReturnOkAsync()
        {
            //Arrange
            var user = _userList.FirstOrDefault();
            _mockAuthenticationService.Setup(x => x.GetCurrentLoginUserAsync(user.UserName)).Returns(user);
            _mockTypingResultService.Setup(x => x.AddAsync(user.Id, _typingResultVm)).Returns(Task.FromResult(_typingResultVm));
            _mockTypingResultService.Setup(x => x.SaveAllAsync()).Returns(Task.FromResult(true));
            CreateSut();

            //Act
            var result = await _sutTypingResultController.PostTypingResultAsync(_typingResultVm);

            //Assert
            ((StatusCodeResult)result).Should().NotBeNull();
            ((StatusCodeResult)result).Should().BeOfType<OkResult>();
        }

        [TestMethod]
        public async Task PostTypingResultAsync_DuplicateKeyException_ReturnsBadRequest()
        {
            var exception = new Exception("Outer Exception", new Exception($"Inner Exception {Constants.Exception.TypingResultError.BadRequestCreateDuplicateKey} and stuff."));
            //Arrange
            var user = _userList.FirstOrDefault();
            _mockAuthenticationService.Setup(x => x.GetCurrentLoginUserAsync(user.UserName)).Returns(user);
            _mockTypingResultService.Setup(x => x.AddAsync(user.Id, _typingResultVm)).Returns(Task.FromResult(_typingResultVm));
            _mockTypingResultService.Setup(x => x.SaveAllAsync()).Throws(exception);
            CreateSut();

            //Act
            var result = await _sutTypingResultController.PostTypingResultAsync(_typingResultVm);

            //Assert
            ((BadRequestObjectResult)result).Value.Should().NotBeNull();
            ((BadRequestObjectResult)result).Value.Should().Be(Constants.Exception.TypingResultError.BadRequestCreateDuplicateKey);
        }

        [TestMethod]
        public async Task PostTypingResultAsync_InvalidUserName_ReturnValidationUserNameRequiredAsync()
        {
            //Arrange
            var user = _userList.FirstOrDefault();
            _mockAuthenticationService.Setup(x => x.GetCurrentLoginUserAsync(null)).Returns(user);
            _mockTypingResultService.Setup(x => x.AddAsync(user.Id, _typingResultVm)).Returns(Task.FromResult(_typingResultVm));
            _mockTypingResultService.Setup(x => x.SaveAllAsync()).Returns(Task.FromResult(true));
            CreateSut();

            //Act
            var result = await _sutTypingResultController.PostTypingResultAsync(_typingResultVm);

            //Assert
            ((BadRequestObjectResult)result).Value.Should().Be(Constants.Exception.Common.ValidationUserNameRequired);
        }

        [TestInitialize]
        public void Setup()
        {
            GenerateMockData();
        }

        private void GenerateMockData()
        {
            _userList = TestHelper.GenerateApplicationUserList();
            _typingResultVm = TestHelper.GenerateTypingResultVmList().FirstOrDefault();
        }

        private void CreateSut()
        {
            _sutTypingResultController = new TypingResultController(_mockTypingResultService.Object, _mockAuthenticationService.Object);
        }

        private static void AutoMapperInit()
        {
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<TypingResultVm, TypingResult>();
            });
            IMapper mapper = config.CreateMapper();
        }
    }
}
