﻿using AutoMapper;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Controllers;
using Web.Models;
using Web.Services;
using Web.ViewModels;

namespace Web.Test.Controllers
{
    [TestClass]
    public class PracticeTextControllerTest
    {
        private PracticeTextController _sutPracticeTextController;
        private Mock<IPracticeTextService> _mockPracticeTextService;
        private Mock<IAuthenticationService> _mockAuthenticationService;

        private PracticeTextVm _practiceTextVm;
        private PracticeTextVm _newAddedPracticeText;
        private ICollection<PracticeTextVm> _practiceTextVmList;
        private List<ApplicationUser> _userList;
        public PracticeTextControllerTest()
        {
            _mockPracticeTextService = new Mock<IPracticeTextService>();
            _mockAuthenticationService = new Mock<IAuthenticationService>();
            AutoMapperInit();
        }

        [TestInitialize]
        public void Setup()
        {
            GenerateMockData();
        }

        [TestMethod]
        public async Task GetAllPracticeTextForAUserAsync_GetAllPracticeTextOfAUser_AllPracticeTextsOfAUserReturnedAsync()
        {
            //Arrange
            var user = _userList.FirstOrDefault();
            _mockAuthenticationService.Setup(x => x.GetCurrentLoginUserAsync(user.UserName)).Returns(user);
            _mockPracticeTextService.Setup(x => x.GetAllPracticeTextForAUserAsync(user.Id)).Returns(Task.FromResult(_practiceTextVmList));
            CreateSut();

            //Act
            var result = await _sutPracticeTextController.GetAllPracticeTextForAUserAsync(user.UserName);

            //Assert
            result.Should().BeOfType<OkObjectResult>();
        }

        [TestMethod]
        public async Task GetAllPracticeTextForAUserAsync_InvalidUserName_ReturnValidationUserNameRequiredAsync()
        {
            //Arrange
            var user = _userList.FirstOrDefault();
            _mockAuthenticationService.Setup(x => x.GetCurrentLoginUserAsync(user.UserName)).Returns(user);
            _mockPracticeTextService.Setup(x => x.GetAllPracticeTextForAUserAsync(user.Id)).Returns(Task.FromResult(_practiceTextVmList));
            CreateSut();

            //Act
            var result = await _sutPracticeTextController.GetAllPracticeTextForAUserAsync(null);

            //Assert
            ((BadRequestObjectResult)result).Value.Should().Be(Constants.Exception.Common.ValidationUserNameRequired);
        }

        [TestMethod]
        public async Task GetAPracticeTextForAUserAsync_GetAPracticeTextForAUserAsync_APracticeTextReturnedAsync()
        {
            //Arrange
            var user = _userList.FirstOrDefault();
            _mockAuthenticationService.Setup(x => x.GetCurrentLoginUserAsync(user.UserName)).Returns(user);
            _mockPracticeTextService.Setup(x => x.GetAPracticeTextForAUserAsync(user.Id, _practiceTextVm.Id)).Returns(Task.FromResult(_practiceTextVm));
            CreateSut();

            //Act
            var result = await _sutPracticeTextController.GetAPracticeTextForAUserAsync(user.UserName, _practiceTextVm.Id.ToString());

            //Assert
            ((OkObjectResult)result).Value.Should().NotBeNull();
            ((OkObjectResult)result).Value.Should().BeOfType<PracticeTextVm>();
            ((PracticeTextVm)((OkObjectResult)result).Value).Id.Should().Be(_practiceTextVm.Id);
        }

        [TestMethod]
        public async Task GetAPracticeTextForAUserAsync_InvalidUserName_ReturnValidationUserNameRequiredAsync()
        {
            //Arrange
            var user = _userList.FirstOrDefault();
            _mockAuthenticationService.Setup(x => x.GetCurrentLoginUserAsync(user.UserName)).Returns(user);
            _mockPracticeTextService.Setup(x => x.GetAPracticeTextForAUserAsync(user.Id, _practiceTextVm.Id)).Returns(Task.FromResult(_practiceTextVm));
            CreateSut();

            //Act
            var result = await _sutPracticeTextController.GetAPracticeTextForAUserAsync(null, _practiceTextVm.Id.ToString());

            //Assert
            ((BadRequestObjectResult)result).Value.Should().Be(Constants.Exception.Common.ValidationUserNameRequired);
        }

        [TestMethod]
        public async Task GetAPracticeTextForAUserAsync_BadPracticeTextId_ReturnPracticeTextNotFoundAsync()
        {
            //Arrange
            var user = _userList.FirstOrDefault();
            _mockAuthenticationService.Setup(x => x.GetCurrentLoginUserAsync(user.UserName)).Returns(user);
            _mockPracticeTextService.Setup(x => x.GetAPracticeTextForAUserAsync(user.Id, _practiceTextVm.Id)).Returns(Task.FromResult(_practiceTextVm));
            CreateSut();

            //Act
            var result = await _sutPracticeTextController.GetAPracticeTextForAUserAsync(user.UserName, "0f8fad5b-d9cb-469f-a165-70867728950e");

            //Assert
            result.Should().BeOfType<NotFoundObjectResult>();
            ((NotFoundObjectResult)result).Value.Should().Be(Constants.Exception.PracticeTextError.NotFound);
        }

        [TestMethod]
        public async Task GetBestPracticeTextForAUserAsync_GetBestPracticeTextForAUserAsync_BestPracticeTextsReturnedAsync()
        {
            //Arrange
            var user = _userList.FirstOrDefault();
            _mockAuthenticationService.Setup(x => x.GetCurrentLoginUserAsync(user.UserName)).Returns(user);
            _mockPracticeTextService.Setup(x => x.GetBestPracticeTextForAUserAsync(user.Id, 3)).Returns(Task.FromResult(_practiceTextVmList));
            CreateSut();

            //Act
            var result = await _sutPracticeTextController.GetBestPracticeTextForAUserAsync(user.UserName, 3);

            //Assert
            result.Should().BeOfType<OkObjectResult>();
        }

        [TestMethod]
        public async Task GetBestPracticeTextForAUserAsync_InvalidUserName_ReturnValidationUserNameRequiredAsync()
        {
            //Arrange
            var user = _userList.FirstOrDefault();
            _mockAuthenticationService.Setup(x => x.GetCurrentLoginUserAsync(user.UserName)).Returns(user);
            _mockPracticeTextService.Setup(x => x.GetBestPracticeTextForAUserAsync(user.Id, 3)).Returns(Task.FromResult(_practiceTextVmList));
            CreateSut();

            //Act
            var result = await _sutPracticeTextController.GetBestPracticeTextForAUserAsync(null, 3);

            //Assert
            ((BadRequestObjectResult)result).Value.Should().Be(Constants.Exception.Common.ValidationUserNameRequired);
        }

        [TestMethod]
        public async Task PostPracticeTextAsync_ValidPracticeText_ReturnOkAsync()
        {
            //Arrange
            var user = _userList.FirstOrDefault();
            _mockAuthenticationService.Setup(x => x.GetCurrentLoginUserAsync(user.UserName)).Returns(user);
            _mockPracticeTextService.Setup(x => x.AddAsync(user.Id, _newAddedPracticeText)).Returns(Task.FromResult(true));
            _mockPracticeTextService.Setup(x => x.SaveAllAsync()).Returns(Task.FromResult(true));
            _mockPracticeTextService.Setup(x => x.GetAsync(It.IsAny<Guid>())).Returns(Task.FromResult(_newAddedPracticeText));
            CreateSut();

            //Act
            var result = await _sutPracticeTextController.PostPracticeTextAsync(_newAddedPracticeText);

            //Assert
            ((OkObjectResult)result).Should().NotBeNull();
            ((OkObjectResult)result).Value.Should().BeOfType<PracticeTextVm>();
            ((PracticeTextVm)((OkObjectResult)result).Value).Id.Should().Be(_newAddedPracticeText.Id);
        }

        [TestMethod]
        public async Task PostPracticeTextAsync_InvalidUserName_ReturnValidationUserNameRequiredAsync()
        {
            //Arrange
            var user = _userList.FirstOrDefault();
            _mockAuthenticationService.Setup(x => x.GetCurrentLoginUserAsync(null)).Returns(user);
            _mockPracticeTextService.Setup(x => x.AddAsync(user.Id, _newAddedPracticeText)).Returns(Task.FromResult(true));
            _mockPracticeTextService.Setup(x => x.SaveAllAsync()).Returns(Task.FromResult(true));
            _mockPracticeTextService.Setup(x => x.GetAsync(It.IsAny<Guid>())).Returns(Task.FromResult(_newAddedPracticeText));
            CreateSut();

            //Act
            var result = await _sutPracticeTextController.PostPracticeTextAsync(_newAddedPracticeText);

            //Assert
            ((BadRequestObjectResult)result).Value.Should().Be(Constants.Exception.Common.ValidationUserNameRequired);
        }

        [TestMethod]
        public async Task PostPracticeTextAsync_DuplicateKeyException_ReturnsBadRequest()
        {
            var exception = new Exception("Outer Exception", new Exception($"Inner Exception {Constants.Exception.PracticeTextError.BadRequestCreateDuplicateKey} and stuff."));
            //Arrange
            var user = _userList.FirstOrDefault();
            _mockAuthenticationService.Setup(x => x.GetCurrentLoginUserAsync(user.UserName)).Returns(user);
            _mockPracticeTextService.Setup(x => x.AddAsync(user.Id, _newAddedPracticeText)).Returns(Task.FromResult(true));
            _mockPracticeTextService.Setup(x => x.SaveAllAsync()).Throws(exception);
            _mockPracticeTextService.Setup(x => x.GetAsync(It.IsAny<Guid>())).Returns(Task.FromResult(_newAddedPracticeText));
            CreateSut();

            //Act
            var result = await _sutPracticeTextController.PostPracticeTextAsync(_newAddedPracticeText);

            //Assert
            ((BadRequestObjectResult)result).Value.Should().NotBeNull();
            ((BadRequestObjectResult)result).Value.Should().Be(Constants.Exception.PracticeTextError.BadRequestCreateDuplicateKey);
        }

        [TestMethod]
        public async Task UpdatePracticeTextAsync_ValidPracticeText_APracticeTextReturnedAsync()
        {
            //Arrange
            var user = _userList.FirstOrDefault();
            _mockAuthenticationService.Setup(x => x.GetCurrentLoginUserAsync(user.UserName)).Returns(user);
            _mockPracticeTextService.Setup(x => x.GetAPracticeTextForAUserAsync(user.Id, _practiceTextVm.Id)).Returns(Task.FromResult(_practiceTextVm));
            _mockPracticeTextService.Setup(x => x.UpdatePracticeTextAsync(user.Id, _practiceTextVm.Id, _practiceTextVm)).Returns(Task.FromResult(Task.FromResult(true)));
            _mockPracticeTextService.Setup(x => x.SaveAllAsync()).Returns(Task.FromResult(true));
            _mockPracticeTextService.Setup(x => x.GetAsync(It.IsAny<Guid>())).Returns(Task.FromResult(_practiceTextVm));
            CreateSut();

            //Act
            var result = await _sutPracticeTextController.UpdatePracticeTextAsync(user.UserName, _practiceTextVm.Id.ToString(), _practiceTextVm);

            //Assert
            ((OkObjectResult)result).Value.Should().NotBeNull();
            ((OkObjectResult)result).Value.Should().BeOfType<PracticeTextVm>();
            ((PracticeTextVm)((OkObjectResult)result).Value).Id.Should().Be(_practiceTextVm.Id);
        }

        [TestMethod]
        public async Task UpdatePracticeTextAsync_InvalidUserName_ReturnValidationUserNameRequiredAsync()
        {
            //Arrange
            var user = _userList.FirstOrDefault();
            _mockAuthenticationService.Setup(x => x.GetCurrentLoginUserAsync(null)).Returns(user);
            _mockPracticeTextService.Setup(x => x.GetAPracticeTextForAUserAsync(user.Id, _practiceTextVm.Id)).Returns(Task.FromResult(_practiceTextVm));
            _mockPracticeTextService.Setup(x => x.UpdatePracticeTextAsync(user.Id, _practiceTextVm.Id, _practiceTextVm)).Returns(Task.FromResult(true));
            _mockPracticeTextService.Setup(x => x.SaveAllAsync()).Returns(Task.FromResult(true));
            _mockPracticeTextService.Setup(x => x.GetAsync(It.IsAny<Guid>())).Returns(Task.FromResult(_practiceTextVm));
            CreateSut();

            //Act
            var result = await _sutPracticeTextController.UpdatePracticeTextAsync(user.UserName, _practiceTextVm.Id.ToString(), _practiceTextVm);

            //Assert
            ((BadRequestObjectResult)result).Value.Should().Be(Constants.Exception.Common.ValidationUserNameRequired);
        }

        [TestMethod]
        public async Task UpdatePracticeTextAsync_BadPracticeTextId_ReturnPracticeTextNotFoundAsync()
        {
            //Arrange
            var user = _userList.FirstOrDefault();
            _mockAuthenticationService.Setup(x => x.GetCurrentLoginUserAsync(user.UserName)).Returns(user);
            _mockPracticeTextService.Setup(x => x.GetAPracticeTextForAUserAsync(user.Id, _practiceTextVm.Id)).Returns(Task.FromResult(_practiceTextVm));
            _mockPracticeTextService.Setup(x => x.UpdatePracticeTextAsync(user.Id, _practiceTextVm.Id, _practiceTextVm)).Returns(Task.FromResult((true)));
            _mockPracticeTextService.Setup(x => x.SaveAllAsync()).Returns(Task.FromResult(true));
            _mockPracticeTextService.Setup(x => x.GetAsync(It.IsAny<Guid>())).Returns(Task.FromResult(_practiceTextVm));
            CreateSut();

            //Act
            var result = await _sutPracticeTextController.UpdatePracticeTextAsync(user.UserName, "0f8fad5b-d9cb-469f-a165-70867728950e", _practiceTextVm);

            //Assert
            result.Should().BeOfType<NotFoundObjectResult>();
            ((NotFoundObjectResult)result).Value.Should().Be(Constants.Exception.PracticeTextError.NotFound);
        }

        [TestMethod]
        public async Task DeletePracticeTextAsync_ValidPracticeTextId_AllPracticeTextsOfAUserReturnedAsync()
        {
            //Arrange
            var user = _userList.FirstOrDefault();
            _mockAuthenticationService.Setup(x => x.GetCurrentLoginUserAsync(user.UserName)).Returns(user);
            _mockPracticeTextService.Setup(x => x.GetAllPracticeTextForAUserAsync(user.Id)).Returns(Task.FromResult(_practiceTextVmList));
            _mockPracticeTextService.Setup(x => x.DeletePracticeTextAsync(user.Id, _practiceTextVm.Id)).Returns(Task.FromResult(true));
            _mockPracticeTextService.Setup(x => x.SaveAllAsync()).Returns(Task.FromResult(true));
            CreateSut();

            //Act
            var result = await _sutPracticeTextController.DeletePracticeTextAsync(user.UserName, _practiceTextVm.Id.ToString());

            //Assert
            result.Should().BeOfType<OkObjectResult>();
        }

        [TestMethod]
        public async Task DeletePracticeTextAsync_InvalidUserName_ReturnValidationUserNameRequiredAsync()
        {
            //Arrange
            var user = _userList.FirstOrDefault();
            _mockAuthenticationService.Setup(x => x.GetCurrentLoginUserAsync(null)).Returns(user);
            _mockPracticeTextService.Setup(x => x.GetAllPracticeTextForAUserAsync(user.Id)).Returns(Task.FromResult(_practiceTextVmList));
            _mockPracticeTextService.Setup(x => x.DeletePracticeTextAsync(user.Id, _practiceTextVm.Id)).Returns(Task.FromResult(true));
            _mockPracticeTextService.Setup(x => x.SaveAllAsync()).Returns(Task.FromResult(true));
            CreateSut();

            //Act
            var result = await _sutPracticeTextController.DeletePracticeTextAsync(user.UserName, _practiceTextVm.Id.ToString());

            //Assert
            ((BadRequestObjectResult)result).Value.Should().Be(Constants.Exception.Common.ValidationUserNameRequired);
        }

        private void GenerateMockData()
        {
            _userList = TestHelper.GenerateApplicationUserList();
            _practiceTextVmList = TestHelper.GeneratePracticeTextVmList();
            _practiceTextVm = _practiceTextVmList.FirstOrDefault();
            _newAddedPracticeText = TestHelper.GenerateANewAddedPracticeTextVm();
        }

        private void CreateSut()
        {
            _sutPracticeTextController = new PracticeTextController(_mockPracticeTextService.Object, _mockAuthenticationService.Object);
        }

        private static void AutoMapperInit()
        {
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<PracticeTextVm, PracticeText>();
            });
            IMapper mapper = config.CreateMapper();
        }
    }
}
