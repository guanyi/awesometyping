﻿using AutoMapper;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Models;
using Web.Repositories;
using Web.Services;
using Web.ViewModels;

namespace Web.Test.Services
{
    [TestClass]
    public class PracticeTextServiceTest
    {
        private readonly int numberOfHighestMistakeRatioSumSelected = 3;
        private IPracticeTextService _sutPracticeTextService;
        private Mock<ICheckedCharacterService> _mockCheckedCharacterService;
        private Mock<IPracticeTextRepository> _mockPracticeTextRepository;
        private Mock<ITypingResultRepository> _mockTypingResultRepository;
        private IMapper _mapper;

        public PracticeTextServiceTest()
        {
            _mockCheckedCharacterService = new Mock<ICheckedCharacterService>();
            _mockPracticeTextRepository = new Mock<IPracticeTextRepository>();
            _mockTypingResultRepository = new Mock<ITypingResultRepository>();
            AutoMapperInit();
            //GenerateMockData();
        }

        [TestMethod]
        public async Task AddAsync_ValidParameter_APracticeTextAddedAsync()
        {
            var practiceTextList = TestHelper.GeneratePracticeTextList();
            var practiceText = practiceTextList.FirstOrDefault();
            var practiceTextVm = TestHelper.GeneratePracticeTextVmList().FirstOrDefault();
            //Arrange
            _mockPracticeTextRepository.Setup(x => x.AddAsync(practiceText))
                .Returns(Task.FromResult(true));
            _mockCheckedCharacterService.Setup(x => x.CreateCheckedCharacterSetAsync(practiceText, It.IsAny<Guid>().ToString()))
                .Returns(Task.FromResult(true));
            CreateSut();

            //Act
            await _sutPracticeTextService.AddAsync(Guid.NewGuid().ToString(), practiceTextVm);

            //Assert
            _mockPracticeTextRepository.Verify(x => x.AddAsync(It.IsAny<PracticeText>()), Times.Once);
        }

        [TestMethod]
        public async Task GetAsync_ValidPracticeTextId_APracticeTextVmReturnedAsync()
        {
            var practiceTextList = TestHelper.GeneratePracticeTextList();
            var practiceText = practiceTextList.FirstOrDefault();
            //Arrange
            _mockPracticeTextRepository.Setup(x => x.GetPracticeTextAsync(It.IsAny<Guid>()))
                .Returns(Task.FromResult(practiceTextList.Where(x=>x.Id == practiceText.Id).FirstOrDefault()));
            CreateSut();

            //Act
            var result = await _sutPracticeTextService.GetAsync(practiceText.Id);

            //Assert
            result.Should().NotBeNull();
            result.Should().BeOfType<PracticeTextVm>();
            result.Id.Should().Be(practiceText.Id);
        }

        [TestMethod]
        public async Task GetAsync_InValidPracticeTextId_APracticeTextVmReturnedAsync()
        {
            //Arrange
            _mockPracticeTextRepository.Setup(x => x.GetPracticeTextAsync(It.IsAny<Guid>()))
                .Returns(Task.FromResult<PracticeText>(null));
            CreateSut();

            //Act
            var result = await _sutPracticeTextService.GetAsync(Guid.NewGuid());

            //Assert
            result.Should().BeNull();
        }

        [TestMethod]
        public async Task SaveAllAsync_CallSave_CallRepoSaveAllAsync()
        {
            //Arrange
            _mockPracticeTextRepository.Setup(x => x.SaveAllAsync()).Returns(Task.FromResult(true));
            CreateSut();

            //Act
            await _sutPracticeTextService.SaveAllAsync();

            //Assert
            _mockPracticeTextRepository.Verify(x => x.SaveAllAsync(), Times.Once);
        }

        [TestMethod]
        public async Task GetAllPracticeTextForAUserAsync_ValidUserId_AListOfPracticeTextVmReturnedAsync()
        {
            var practiceTextList = TestHelper.GeneratePracticeTextList();
            var user = TestHelper.GenerateApplicationUserList().FirstOrDefault();
            //Arrange
            _mockPracticeTextRepository.Setup(x => x.GetAllPracticeTextForAUserAsync(user.Id.ToString()))
                .Returns(Task.FromResult(practiceTextList.Where(x=>x.UserId == user.Id)));
            CreateSut();

            //Act
            var result = await _sutPracticeTextService.GetAllPracticeTextForAUserAsync(user.Id.ToString());

            //Assert
            result.Should().NotBeNull();
            result.Should().BeOfType<List<PracticeTextVm>>();
        }

        [TestMethod]
        public async Task GetAllPracticeTextForAUserAsync_InValidUserId_AListOfPracticeTextVmReturnedAsync()
        {
            var practiceTextList = TestHelper.GeneratePracticeTextList();
            var user = TestHelper.GenerateApplicationUserList().FirstOrDefault();
            //Arrange
            _mockPracticeTextRepository.Setup(x => x.GetAllPracticeTextForAUserAsync(It.IsAny<Guid>().ToString()))
                .Returns(Task.FromResult<IEnumerable<PracticeText>>(null));
            CreateSut();

            //Act
            var result = await _sutPracticeTextService.GetAllPracticeTextForAUserAsync(Guid.NewGuid().ToString());

            //Assert
            result.Count.Should().Be(0);
        }

        [TestMethod]
        public async Task GetAPracticeTextForAUserAsync_ValidUserIdAndPracticeTextId_APracticeTextVmReturnedAsync()
        {
            var practiceText = TestHelper.GeneratePracticeTextList().FirstOrDefault();
            //Arrange
            _mockPracticeTextRepository.Setup(x => x.GetAPracticeTextForAUserAsync(practiceText.UserId, practiceText.Id))
                .Returns(Task.FromResult(practiceText));
            CreateSut();

            //Act
            var result = await _sutPracticeTextService.GetAPracticeTextForAUserAsync(practiceText.UserId, practiceText.Id);

            //Assert
            result.Should().NotBeNull();
            result.Should().BeOfType<PracticeTextVm>();
            result.Id.Should().Be(practiceText.Id);
        }

        [TestMethod]
        public async Task GetAPracticeTextForAUserAsync_InValidUserIdAndValidPracticeTextId_APracticeTextVmReturnedAsync()
        {
            var practiceText = TestHelper.GeneratePracticeTextList().FirstOrDefault();
            //Arrange
            _mockPracticeTextRepository.Setup(x => x.GetAPracticeTextForAUserAsync(It.IsAny<Guid>().ToString(), practiceText.Id))
                .Returns(Task.FromResult<PracticeText>(null));
            CreateSut();

            //Act
            var result = await _sutPracticeTextService.GetAPracticeTextForAUserAsync(Guid.NewGuid().ToString(), practiceText.Id);

            //Assert
            result.Should().BeNull();
        }

        [TestMethod]
        public async Task GetAPracticeTextForAUserAsync_ValidUserIdAndInValidPracticeTextId_APracticeTextVmReturnedAsync()
        {
            var practiceText = TestHelper.GeneratePracticeTextList().FirstOrDefault();
            //Arrange
            _mockPracticeTextRepository.Setup(x => x.GetAPracticeTextForAUserAsync(practiceText.UserId, It.IsAny<Guid>()))
                .Returns(Task.FromResult<PracticeText>(null));
            CreateSut();

            //Act
            var result = await _sutPracticeTextService.GetAPracticeTextForAUserAsync(practiceText.UserId, Guid.NewGuid());

            //Assert
            result.Should().BeNull();
        }

        [TestMethod]
        public async Task GetBestPracticeTextForAUserAsync_ValidUserId_AListOfPracticeTextVmReturnedAsync()
        {
            var practiceText = TestHelper.GeneratePracticeTextList().FirstOrDefault();
            var typingResultList = TestHelper.GenerateTypingResultList();
            //Arrange
            _mockCheckedCharacterService.Setup(x => x.GetPracticeTextIdOfHighestPercentageCharacterAsync(It.IsAny<string>()))
                .Returns(Task.FromResult(practiceText.Id));
            _mockPracticeTextRepository.Setup(x => x.GetAPracticeTextForAUserAsync(practiceText.UserId, practiceText.Id))
                .Returns(Task.FromResult(practiceText));
            _mockTypingResultRepository.Setup(x => x.GetAllTypingResultsForAUserAsync(practiceText.UserId))
                .Returns(Task.FromResult(typingResultList));
            CreateSut();

            //Act
            var result = await _sutPracticeTextService.GetBestPracticeTextForAUserAsync(practiceText.UserId, It.IsAny<int>());

            //Assert
            result.Should().NotBeNull();
        }

        [TestMethod]
        public async Task UpdatePracticeTextAsync_ValidUserIdAndValidPracticeTextId_PracticeTextUpdatedAsync()
        {
            var practiceTextVm = TestHelper.GeneratePracticeTextVmList().FirstOrDefault();
            var practiceText = TestHelper.GeneratePracticeTextList().FirstOrDefault();
            var user = TestHelper.GenerateApplicationUserList().FirstOrDefault();

            //Arrange
            _mockPracticeTextRepository.Setup(x => x.GetAPracticeTextForAUserAsync(user.Id, practiceText.Id))
                .Returns(Task.FromResult(practiceText));
            _mockPracticeTextRepository.Setup(x => x.Update(practiceText));
            _mockCheckedCharacterService.Setup(x => x.UpdateCheckedCharacterSet(practiceText));
            CreateSut();

            //Act
            await _sutPracticeTextService.UpdatePracticeTextAsync(user.Id, practiceTextVm.Id, practiceTextVm);

            //Assert
            _mockPracticeTextRepository.Verify(x => x.Update(practiceText), Times.Once);
        }

        [TestMethod]
        public async Task DeletePracticeTextAsync_ValidUserIdAndValidPracticeTextId_PracticeTextDeletedAsync()
        {
            var practiceText = TestHelper.GeneratePracticeTextList().FirstOrDefault();
            var user = TestHelper.GenerateApplicationUserList().FirstOrDefault();

            //Arrange
            _mockPracticeTextRepository.Setup(x => x.GetAPracticeTextForAUserAsync(user.Id, practiceText.Id))
                .Returns(Task.FromResult(practiceText));
            _mockPracticeTextRepository.Setup(x => x.Update(practiceText));
            CreateSut();

            //Act
            await _sutPracticeTextService.DeletePracticeTextAsync(user.Id, practiceText.Id);

            //Assert
            _mockPracticeTextRepository.Verify(x => x.Update(practiceText), Times.Once);
        }

        private void AutoMapperInit()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<PracticeTextVm, PracticeText>().ReverseMap();
            });
            _mapper = config.CreateMapper();
        }

        private void CreateSut()
        {
            _sutPracticeTextService = new PracticeTextService(_mockCheckedCharacterService.Object, _mockPracticeTextRepository.Object, _mockTypingResultRepository.Object, _mapper);
        }
    }
}
