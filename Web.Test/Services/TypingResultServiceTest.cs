﻿using AutoMapper;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Models;
using Web.Repositories;
using Web.Services;
using Web.ViewModels;
namespace Web.Test.Services
{
    [TestClass]
    public class TypingResultServiceTest
    {
        private ITypingResultService _sutTypingResultService;
        private Mock<ITypingResultRepository> _mockTypingResultRepository;
        private Mock<ITypingResultDetailRepository> _mockTypingResultDetailRepository;
        private Mock<ICheckedCharacterRepository> _mockCheckedCharacterRepository;
        private TypingResultVm _typingResultVm;

        public TypingResultServiceTest()
        {
            _mockTypingResultRepository = new Mock<ITypingResultRepository>();
            _mockTypingResultDetailRepository = new Mock<ITypingResultDetailRepository>();
            _mockCheckedCharacterRepository = new Mock<ICheckedCharacterRepository>();
        }

        [TestMethod]
        public async Task AddAsync_ValidTypingResult_CallRepoAddAsync()
        {
            var checkedCharacters = new List<CheckedCharacter>();
            //Arrange
            _mockTypingResultRepository.Setup(x => x.AddAsync(It.IsAny<TypingResult>())).Returns(Task.FromResult(Task.FromResult(true)));
            _mockTypingResultDetailRepository.Setup(x => x.SaveAllAsync()).Returns(Task.FromResult(true));
            _mockCheckedCharacterRepository.Setup(x => x.GetCheckedCharactersOfAPracticeTextAsync(It.IsAny<Guid>())).Returns(Task.FromResult(checkedCharacters));
            CreateSut();

            //Act
            await _sutTypingResultService.AddAsync(Guid.NewGuid().ToString(), _typingResultVm);

            //Assert
            _mockTypingResultRepository.Verify(x => x.AddAsync(It.IsAny<TypingResult>()), Times.Once);
        }

        [TestMethod]
        public async Task SaveAllAsync_CallSave_CallRepoSaveAllAsync()
        {
            //Arrange
            _mockTypingResultRepository.Setup(x => x.SaveAllAsync()).Returns(Task.FromResult(true));
            CreateSut();

            //Act
            await _sutTypingResultService.SaveAllAsync();

            //Assert
            _mockTypingResultRepository.Verify(x => x.SaveAllAsync(), Times.Once);
        }

        [TestInitialize]
        public void Setup()
        {
            GenerateMockData();
        }

        private void GenerateMockData()
        {
            _typingResultVm = TestHelper.GenerateTypingResultVmList().FirstOrDefault();
        }

        private void CreateSut()
        {
            _sutTypingResultService = new TypingResultService(_mockTypingResultRepository.Object, _mockTypingResultDetailRepository.Object, _mockCheckedCharacterRepository.Object);
        }
    }
}
