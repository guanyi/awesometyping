﻿using AutoMapper;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Models;
using Web.Repositories;
using Web.Services;
using Web.ViewModels;

namespace Web.Test.Services
{
    [TestClass]
    public class AuthenticationServiceTest
    {
        private IAuthenticationService _sutAuthenticationService;
        private Mock<IApplicationUserRepository> _mockApplicationUserRepository;
        private List<ApplicationUser> _userList;
        private ApplicationUser _user;

        public AuthenticationServiceTest()
        {
            _mockApplicationUserRepository = new Mock<IApplicationUserRepository>();
        }

        [TestMethod]
        public void GetCurrentLoginUserAsync_ValidPracticeText_CallAddRangeAsync()
        {
            //Arrange
            _mockApplicationUserRepository.Setup(x => x.GetAllUsers()).Returns(_userList.AsQueryable());
            CreateSut();

            //Act
            var result = _sutAuthenticationService.GetCurrentLoginUserAsync(_user.UserName);

            //Assert
            result.Should().NotBeNull();
            result.Id.Should().Be(_user.Id);
        }

        [TestInitialize]
        public void Setup()
        {
            GenerateMockData();
        }

        private void GenerateMockData()
        {
            _userList = TestHelper.GenerateApplicationUserList();
            _user = _userList.FirstOrDefault();
        }

        private void CreateSut()
        {
            _sutAuthenticationService = new AuthenticationService(_mockApplicationUserRepository.Object);
        }
    }
}
