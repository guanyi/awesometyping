﻿using AutoMapper;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Models;
using Web.Repositories;
using Web.Services;
using Web.ViewModels;

namespace Web.Test.Services
{
    [TestClass]
    public class CheckedCharacterServiceTest
    {
        private ICheckedCharacterService _sutCheckedCharacterService;
        private Mock<ICheckedCharacterRepository> _mockCheckedCharacterRepository;
        private PracticeText _practiceText;

        public CheckedCharacterServiceTest()
        {
            _mockCheckedCharacterRepository = new Mock<ICheckedCharacterRepository>();
        }

        [TestMethod]
        public async Task CreateCheckedCharacterSetAsync_ValidPracticeText_CallAddRangeAsync()
        {
            var checkedCharacters = new List<CheckedCharacter>();
            //Arrange
            _mockCheckedCharacterRepository.Setup(x => x.AddRangeAsync(It.IsAny<List<CheckedCharacter>>())).Returns(Task.FromResult(true));
            CreateSut();

            //Act
            await _sutCheckedCharacterService.CreateCheckedCharacterSetAsync(_practiceText, Guid.NewGuid().ToString());

            //Assert
            _mockCheckedCharacterRepository.Verify(x => x.AddRangeAsync(It.IsAny<List<CheckedCharacter>>()), Times.Once);
        }

        [TestMethod]
        public void UpdateCheckedCharacterSet_ValidPracticeText_CallUpdateRangeAsync()
        {
            _practiceText.CheckedCharacters = TestHelper.GenerateCheckedCharacterList();
            //Arrange
            _mockCheckedCharacterRepository.Setup(x => x.UpdateRange(It.IsAny<List<CheckedCharacter>>()));
            CreateSut();

            //Act
            _sutCheckedCharacterService.UpdateCheckedCharacterSet(_practiceText);

            //Assert
            _mockCheckedCharacterRepository.Verify(x => x.UpdateRange(It.IsAny<List<CheckedCharacter>>()), Times.Once);
        }

        [TestMethod]
        public void DeleteCheckedCharacterSet_ValidCheckedCharacterList_CallUpdateRangeAsync()
        {
            var checkedCharacters = TestHelper.GenerateCheckedCharacterList();
            //Arrange
            _mockCheckedCharacterRepository.Setup(x => x.UpdateRange(It.IsAny<List<CheckedCharacter>>()));
            CreateSut();

            //Act
            _sutCheckedCharacterService.DeleteCheckedCharacterSet(checkedCharacters);

            //Assert
            _mockCheckedCharacterRepository.Verify(x => x.UpdateRange(It.IsAny<List<CheckedCharacter>>()), Times.Once);
        }

        [TestMethod]
        public async Task SaveAllAsync_CallSave_CallRepoSaveAllAsync()
        {
            //Arrange
            _mockCheckedCharacterRepository.Setup(x => x.SaveAllAsync()).Returns(Task.FromResult(true));
            CreateSut();

            //Act
            await _sutCheckedCharacterService.SaveAllAsync();

            //Assert
            _mockCheckedCharacterRepository.Verify(x => x.SaveAllAsync(), Times.Once);
        }

        [TestInitialize]
        public void Setup()
        {
            GenerateMockData();
        }

        private void GenerateMockData()
        {
            _practiceText = TestHelper.GeneratePracticeTextList().FirstOrDefault();
        }

        private void CreateSut()
        {
            _sutCheckedCharacterService = new CheckedCharacterService(_mockCheckedCharacterRepository.Object);
        }
    }
}
