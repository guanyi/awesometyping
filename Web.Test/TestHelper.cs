﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using Web.Models;
using Web.ViewModels;

namespace Web.Test
{
    [TestClass]
    public static class TestHelper
    {
        private static PracticeTextVm _newAddedPracticeText;
        private static List<TypingResult> _typingResultList;
        private static List<TypingResultVm> _typingResultVm;
        private static List<ApplicationUser> _userLists;
        private static List<TypingResultVm> _typingResultVmLists;
        private static List<PracticeTextVm> _practiceTextVmLists;
        private static List<PracticeText> _practiceTextLists;
        private static List<CheckedCharacter> _checkedCharacterLists;

        public static List<ApplicationUser> GenerateApplicationUserList()
        {
            if (_userLists != null) return _userLists;
            _userLists = new List<ApplicationUser>
            {
                new ApplicationUser() {Id = "00000000-0000-0000-0000-000000000100", UserName = "guanyifang@Hotmail.com", EmailConfirmed = false, PhoneNumberConfirmed = false, TwoFactorEnabled = false, LockoutEnabled = true, AccessFailedCount = 0},
                new ApplicationUser() {Id = "00000000-0000-0000-0000-000000000101", UserName = "JoeBlack", EmailConfirmed = false, PhoneNumberConfirmed = false, TwoFactorEnabled = false, LockoutEnabled = true, AccessFailedCount = 0}
            };
            return _userLists;
        }

        public static IEnumerable<TypingResult> GenerateTypingResultList()
        {
            if (_typingResultList != null) return _typingResultList;
            _typingResultList = new List<TypingResult>
            {
                new TypingResult(Guid.NewGuid(), new Guid("00000000-0000-0000-0000-100000000000"), 59, 2, 32.5, 89.6, "00000000-0000-0000-0000-000000000100"),
                new TypingResult(Guid.NewGuid(), new Guid("00000000-0000-0000-0000-100000000000"), 17, 6, 17.9, 72.5, "00000000-0000-0000-0000-000000000100")
            };
            return _typingResultList;
        }

        public static List<TypingResultVm> GenerateTypingResultVmList()
        {
            if (_typingResultVmLists != null) return _typingResultVmLists;
            _typingResultVmLists = new List<TypingResultVm>
            {
                new TypingResultVm() {PracticeTextId = new Guid("00000000-0000-0000-0000-100000000000"), DurationSeconds = 59, MistakeCount = 2, Wpm = 32.5, Accuracy = 89.6, UserName = "guanyifang@Hotmail.com", CreateDate = new DateTime(), TypingResultDetails = new List<TypingResultDetailVm>()},
                new TypingResultVm() {PracticeTextId = new Guid("00000000-0000-0000-0000-100000000001"), DurationSeconds = 17, MistakeCount = 6, Wpm = 17.9, Accuracy = 72.5, UserName = "guanyifang@Hotmail.com", CreateDate = new DateTime(), TypingResultDetails = new List<TypingResultDetailVm>()}
            };
            return _typingResultVmLists;
        }

        public static List<PracticeTextVm> GeneratePracticeTextVmList()
        {
            if (_practiceTextVmLists != null) return _practiceTextVmLists;
            _practiceTextVmLists = new List<PracticeTextVm>
            {
                new PracticeTextVm() {Id = new Guid("00000000-0000-0000-0000-001000000000"), Title = "T1", Text = "Text1", TotalWords = 124, TotalCharacters = 785, IsPublic = true, UserName = "guanyifang@Hotmail.com", ModifiedDate = new DateTime()},
                new PracticeTextVm() {Id = new Guid("00000000-0000-0000-0000-001000000001"), Title = "T2", Text = "Text2", TotalWords = 75, TotalCharacters = 541, IsPublic = true, UserName = "guanyifang@Hotmail.com", ModifiedDate = new DateTime()},
                new PracticeTextVm() {Id = new Guid("00000000-0000-0000-0000-001000000002"), Title = "T3", Text = "Text3", TotalWords = 325, TotalCharacters = 1547, IsPublic = true, UserName = "guanyifang@Hotmail.com", ModifiedDate = new DateTime()}
            };
            return _practiceTextVmLists;
        }

        public static PracticeTextVm GenerateANewAddedPracticeTextVm()
        {
            if (_newAddedPracticeText != null) return _newAddedPracticeText;
            _newAddedPracticeText = new PracticeTextVm() { Id = new Guid("00000000-0000-0000-0000-000000000000"), Title = "New", Text = "TextNew", TotalWords = 19, TotalCharacters = 85, IsPublic = true, UserName = "guanyifang@Hotmail.com", ModifiedDate = new DateTime() };
            return _newAddedPracticeText;
        }

        public static List<PracticeText> GeneratePracticeTextList()
        {
            if (_practiceTextLists != null) return _practiceTextLists;
            _practiceTextLists = new List<PracticeText>
            {
                new PracticeText(new Guid("00000000-0000-0000-0000-001000000000"), "00000000-1000-0000-0000-001000000000", "T1", "Text1", 124, 785, true, true),
                new PracticeText(new Guid("00000000-0000-0000-0000-001000000001"), "00000000-1000-0000-0000-001000000000", "T2", "Text2", 147, 885, true, true),
                new PracticeText(new Guid("00000000-0000-0000-0000-001000000002"), "00000000-1000-0000-0000-001000000000", "T3", "Text3", 24, 89, true, true)
            };
            return _practiceTextLists;
        }

        public static List<CheckedCharacter> GenerateCheckedCharacterList()
        {
            if (_checkedCharacterLists != null) return _checkedCharacterLists;
            _checkedCharacterLists = new List<CheckedCharacter>
            {
                new CheckedCharacter(Guid.NewGuid(), new Guid("00000000-0000-0000-0000-001000000000"), "a", 0.05, true, Guid.NewGuid().ToString()),
                new CheckedCharacter(Guid.NewGuid(), new Guid("00000000-0000-0000-0000-001000000000"), "b", 0.05, true, Guid.NewGuid().ToString()),
                new CheckedCharacter(Guid.NewGuid(), new Guid("00000000-0000-0000-0000-001000000000"), "c", 0.05, true, Guid.NewGuid().ToString())
            };
            return _checkedCharacterLists;
        }
    }
}
